MODULE periodic_table
	USE kinds
	! Periodic table 

	! Table 1 from
	! Atomic weights of the elements 2013 (IUPAC Technical Report)
	! https://doi.org/10.1515/pac-2015-0305"
	! Extracted from PaPIM / e-CAM project Momir Malis   


	IMPLICIT NONE

	PRIVATE
	PUBLIC :: get_atomic_mass,init_periodic_table,get_info_from_xyz_file &
			, get_atomic_index, get_atomic_symbol

	INTEGER, PARAMETER :: N_EL = 119  
	CHARACTER (LEN = 2), ALLOCATABLE, DIMENSION(:), SAVE :: element_symbol 
	REAL (dp)          , ALLOCATABLE, DIMENSION(:), SAVE :: element_mass   

CONTAINS

	subroutine init_periodic_table()
		implicit none

		IF (.NOT. allocated(element_symbol))  &
		allocate(element_symbol(N_EL), element_mass(N_EL))

		! Atomic weights with respect to atomic number     
		element_symbol(001) = "H"
		element_symbol(002) = "He"
		element_symbol(003) = "Li"
		element_symbol(004) = "Be"
		element_symbol(005) = "B"
		element_symbol(006) = "C"
		element_symbol(007) = "N"
		element_symbol(008) = "O"
		element_symbol(009) = "F"
		element_symbol(010) = "Ne"
		element_symbol(011) = "Na"
		element_symbol(012) = "Mg"
		element_symbol(013) = "Al"
		element_symbol(014) = "Si"
		element_symbol(015) = "P"
		element_symbol(016) = "S"
		element_symbol(017) = "Cl"
		element_symbol(018) = "Ar"
		element_symbol(019) = "K"
		element_symbol(020) = "Ca"
		element_symbol(021) = "Sc"
		element_symbol(022) = "Ti"
		element_symbol(023) = "V"
		element_symbol(024) = "Cr"
		element_symbol(025) = "Mn"
		element_symbol(026) = "Fe"
		element_symbol(027) = "Co"
		element_symbol(028) = "Ni"
		element_symbol(029) = "Cu"
		element_symbol(030) = "Zn"
		element_symbol(031) = "Ga"
		element_symbol(032) = "Ge"
		element_symbol(033) = "As"
		element_symbol(034) = "Se"
		element_symbol(035) = "Br"
		element_symbol(036) = "Kr"
		element_symbol(037) = "Rb"
		element_symbol(038) = "Sr"
		element_symbol(039) = "Y"
		element_symbol(040) = "Zr"
		element_symbol(041) = "Nb"
		element_symbol(042) = "Mo"
		element_symbol(043) = "Tc"
		element_symbol(044) = "Ru"
		element_symbol(045) = "Rh"
		element_symbol(046) = "Pd"
		element_symbol(047) = "Ag"
		element_symbol(048) = "Cd"
		element_symbol(049) = "In"
		element_symbol(050) = "Sn"
		element_symbol(051) = "Sb"
		element_symbol(052) = "Te"
		element_symbol(053) = "I"
		element_symbol(054) = "Xe"
		element_symbol(055) = "Cs"
		element_symbol(056) = "Ba"
		element_symbol(057) = "La"
		element_symbol(058) = "Ce"
		element_symbol(059) = "Pr"
		element_symbol(060) = "Nd"
		element_symbol(061) = "Pm"
		element_symbol(062) = "Sm"
		element_symbol(063) = "Eu"
		element_symbol(064) = "Gd"
		element_symbol(065) = "Tb"
		element_symbol(066) = "Dy"
		element_symbol(067) = "Ho"
		element_symbol(068) = "Er"
		element_symbol(069) = "Tm"
		element_symbol(070) = "Yb"
		element_symbol(071) = "Lu"
		element_symbol(072) = "Hf"
		element_symbol(073) = "Ta"
		element_symbol(074) = "W"
		element_symbol(075) = "Re"
		element_symbol(076) = "Os"
		element_symbol(077) = "Ir"
		element_symbol(078) = "Pt"
		element_symbol(079) = "Au"
		element_symbol(080) = "Hg"
		element_symbol(081) = "Tl"
		element_symbol(082) = "Pb"
		element_symbol(083) = "Bi"
		element_symbol(084) = "Po"
		element_symbol(085) = "At"
		element_symbol(086) = "Rn"
		element_symbol(087) = "Fr"
		element_symbol(088) = "Ra"
		element_symbol(089) = "Ac"
		element_symbol(090) = "Th"
		element_symbol(091) = "Pa"
		element_symbol(092) = "U"
		element_symbol(093) = "Np"
		element_symbol(094) = "Pu"
		element_symbol(095) = "Am"
		element_symbol(096) = "Cm"
		element_symbol(097) = "Bk"
		element_symbol(098) = "Cf"
		element_symbol(099) = "Es"
		element_symbol(100) = "Fm"
		element_symbol(101) = "Md"
		element_symbol(102) = "No"
		element_symbol(103) = "Lr"
		element_symbol(104) = "Rf"
		element_symbol(105) = "Db"
		element_symbol(106) = "Sg"
		element_symbol(107) = "Bh"
		element_symbol(108) = "Hs"
		element_symbol(109) = "Mt"
		element_symbol(110) = "Ds"
		element_symbol(111) = "Rg"
		element_symbol(112) = "Cn"
		element_symbol(113) = "Nh"     
		element_symbol(114) = "Fl"      
		element_symbol(115) = "Mc"     
		element_symbol(116) = "Lv"      
		element_symbol(117) = "Ts"     
		element_symbol(118) = "Og"
		element_symbol(119) = "D" !Deuterium

		! Atomic weights with respect to atomic number 
		element_mass(001) =    1.00784
		element_mass(002) =    4.002602
		element_mass(003) =    6.938
		element_mass(004) =    9.0121831
		element_mass(005) =    10.806
		element_mass(006) =    12.0096
		element_mass(007) =    14.00643
		element_mass(008) =    15.99903
		element_mass(009) =    18.998403163
		element_mass(010) =    20.1797
		element_mass(011) =    22.98976928
		element_mass(012) =    24.304
		element_mass(013) =    26.9815385
		element_mass(014) =    28.084
		element_mass(015) =    30.973761998
		element_mass(016) =    32.059
		element_mass(017) =    35.446
		element_mass(018) =    39.948
		element_mass(019) =    39.0983
		element_mass(020) =    40.078
		element_mass(021) =    44.955908
		element_mass(022) =    47.867
		element_mass(023) =    50.9415
		element_mass(024) =    51.9961
		element_mass(025) =    54.938044
		element_mass(026) =    55.845
		element_mass(027) =    58.933194
		element_mass(028) =    58.6934
		element_mass(029) =    63.546
		element_mass(030) =    65.38
		element_mass(031) =    69.723
		element_mass(032) =    72.630
		element_mass(033) =    74.921595
		element_mass(034) =    78.971
		element_mass(035) =    79.901
		element_mass(036) =    83.798
		element_mass(037) =    85.4678
		element_mass(038) =    87.62
		element_mass(039) =    88.90584
		element_mass(040) =    91.224
		element_mass(041) =    92.90637
		element_mass(042) =    95.95
		element_mass(043) =    -999
		element_mass(044) =    101.07
		element_mass(045) =    102.90550
		element_mass(046) =    106.42
		element_mass(047) =    107.8682
		element_mass(048) =    112.414
		element_mass(049) =    114.818
		element_mass(050) =    118.710
		element_mass(051) =    121.760
		element_mass(052) =    127.60
		element_mass(053) =    126.90447
		element_mass(054) =    131.293
		element_mass(055) =    132.90545196
		element_mass(056) =    137.327
		element_mass(057) =    138.90547
		element_mass(058) =    140.116
		element_mass(059) =    140.90766
		element_mass(060) =    144.242
		element_mass(061) =    -999
		element_mass(062) =    150.36
		element_mass(063) =    151.964
		element_mass(064) =    157.25
		element_mass(065) =    158.92535
		element_mass(066) =    162.500
		element_mass(067) =    164.93033
		element_mass(068) =    167.259
		element_mass(069) =    168.93422
		element_mass(070) =    173.054
		element_mass(071) =    174.9668
		element_mass(072) =    178.49
		element_mass(073) =    180.94788
		element_mass(074) =    183.84
		element_mass(075) =    186.207
		element_mass(076) =    190.23
		element_mass(077) =    192.217
		element_mass(078) =    195.084
		element_mass(079) =    196.966569
		element_mass(080) =    200.592
		element_mass(081) =    204.382
		element_mass(082) =    207.2
		element_mass(083) =    208.98040
		element_mass(084) =    -999
		element_mass(085) =    -999
		element_mass(086) =    -999
		element_mass(087) =    -999
		element_mass(088) =    -999
		element_mass(089) =    -999
		element_mass(090) =    232.0377
		element_mass(091) =    231.03588
		element_mass(092) =    238.02891
		element_mass(093) =    -999
		element_mass(094) =    -999
		element_mass(095) =    -999
		element_mass(096) =    -999
		element_mass(097) =    -999
		element_mass(098) =    -999
		element_mass(099) =    -999
		element_mass(100) =    -999
		element_mass(101) =    -999
		element_mass(102) =    -999
		element_mass(103) =    -999
		element_mass(104) =    -999
		element_mass(105) =    -999
		element_mass(106) =    -999
		element_mass(107) =    -999
		element_mass(108) =    -999
		element_mass(109) =    -999
		element_mass(110) =    -999
		element_mass(111) =    -999
		element_mass(112) =    -999
		element_mass(113) =    -999
		element_mass(114) =    -999
		element_mass(115) =    -999
		element_mass(116) =    -999
		element_mass(117) =    -999
		element_mass(118) =    -999
		element_mass(119) =    2._dp*element_mass(001) ! Deuterium

	end subroutine init_periodic_table    

	function get_atomic_mass(element) result(mass)
		implicit none
		integer :: i
		real(dp) :: mass
		character (len = 2) :: element 

		do i = 1 , n_el
			if (element == element_symbol(i)) then
				mass = element_mass(i)         
				if (mass == -999) then
					WRITE(0,*) "Error: no standard atomic weight !"
					STOP "Execution stopped."
				endif
				return
			endif 
		enddo

		WRITE(0,*) element," has not been found in periodic_table !"
		STOP "Execution stopped."

	end function get_atomic_mass

	function get_atomic_index(element) result(index)
		implicit none
		integer :: i
		integer :: index
		character (len = 2) :: element 

		do i = 1 , n_el
			if (element == element_symbol(i)) then
				index=i
				return
			endif 
		enddo

		WRITE(0,*) element," has not been found in periodic_table !"
		STOP "Execution stopped."

	end function get_atomic_index

	function get_atomic_symbol(index) result(element)
		implicit none
		integer :: i
		integer :: index
		character (len = 2) :: element 

		if(index<1 .or. index>n_el) then
			WRITE(0,*) "index",index," is not in periodic_table !"
			STOP "Execution stopped."
		endif
		element=element_symbol(index)

	end function get_atomic_symbol

	subroutine get_info_from_xyz_file(filename,n_atoms,X,mass,num_is_indicated,blank_line,elements,indices)
		IMPLICIT NONE
		CHARACTER(*), INTENT(in) :: filename
		INTEGER, INTENT(out) :: n_atoms
		REAL(dp), allocatable, INTENT(out) :: X(:,:), mass(:)
		LOGICAL, INTENT(in) :: num_is_indicated, blank_line
		CHARACTER(2), allocatable, INTENT(out), OPTIONAL :: elements(:)
		INTEGER, allocatable, INTENT(out), OPTIONAL :: indices(:)		
		LOGICAL :: file_exists
		INTEGER :: u,ios,i,at_num
		CHARACTER(2), allocatable :: tmp(:)

		INQUIRE( FILE=trim(filename), EXIST=file_exists)
		IF( .not. file_exists ) THEN
			write(0,*) "Error: xyz file '"//trim(filename)//"' does not exist!"
			STOP "Execution stopped."
		ENDIF
		OPEN(newunit=u,file=trim(filename),ACTION="READ")
		READ(u,*,iostat=ios) n_atoms
		IF(ios/=0) STOP "Error while reading xyz file at 'n_atoms'! Execution stopped." 

		
		if(n_atoms<=0) STOP "Error: 'n_atoms' is not properly defined!"
		IF(allocated(X)) deallocate(X)
		IF(allocated(mass)) deallocate(mass)
		allocate(X(n_atoms,3) &
				,mass(n_atoms) &
				,tmp(n_atoms))
				
		if(blank_line) READ(u,*,iostat=ios)
		
		IF(num_is_indicated) THEN
			DO i=1,n_atoms
				READ(u,*,iostat=ios) at_num,tmp(i),X(i,:)
				IF(ios/=0) STOP "Error while reading xyz file! Execution stopped." 
				mass(i)=get_atomic_mass(tmp(i))			
			ENDDO
		ELSE
			DO i=1,n_atoms
				READ(u,*,iostat=ios) tmp(i),X(i,:)
				IF(ios/=0) STOP "Error while reading xyz file! Execution stopped." 
				mass(i)=get_atomic_mass(tmp(i))			
			ENDDO
		ENDIF
		CLOSE(u)

		

		IF(present(elements)) then
			IF(allocated(elements)) deallocate(elements)
			allocate(elements(n_atoms))
			elements=tmp
		endif
		IF(present(indices)) then
			IF(allocated(indices)) deallocate(indices)
			allocate(indices(n_atoms))
			DO i=1,n_atoms
				indices(i)=get_atomic_index(tmp(i))
			ENDDO
		endif

	end subroutine get_info_from_xyz_file


END MODULE periodic_table
