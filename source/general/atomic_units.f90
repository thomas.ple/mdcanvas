module atomic_units
	USE kinds
	USE string_operations
	USE nested_dictionaries
	implicit none

	real(dp), PARAMETER :: eV     = 27.211_dp       ! Hartree to eV
	real(dp), PARAMETER :: kcalperMol     = 627.5094736_dp       ! Hartree to kcal/mol
	real(dp), PARAMETER :: bohr   = 0.52917721_dp    ! Bohr to Angstrom
	real(dp), PARAMETER :: Mprot    = 1836.15        ! proton mass
!	real(dp), PARAMETER :: hbar   = 1._dp           ! Planck's constant
	real(dp), PARAMETER :: fs     = 2.4188843e-2_dp ! AU time to femtoseconds
	real(dp), PARAMETER :: kelvin = 3.15774e5_dp    ! Hartree to Kelvin
	real(dp), PARAMETER :: THz = 1000/fs		! AU frequency to THz
	real(dp), PARAMETER :: nNewton = 82.387_dp		! AU Force to nNewton 
	real(dp), PARAMETER :: cm1=219471.52_dp  ! Hartree to cm-1 
	real(dp), PARAMETER :: gmol_Afs=bohr/(Mprot*fs) !AU momentum to (g/mol).A/fs
	real(dp), PARAMETER :: kbar=294210.2648438959 ! Hartree/bohr**3 to kbar
  
  TYPE ATOMIC_UNITS_TYPE
	  real(dp) :: eV     = eV 
	  real(dp) :: kcalperMol     = kcalperMol
	  real(dp) :: bohr   = bohr 
	  real(dp) :: Mprot    = Mprot
	  real(dp) :: fs     = fs 
	  real(dp) :: kelvin = kelvin 
	  real(dp) :: THz = THz 
	  real(dp) :: nNewton = nNewton 
	  real(dp) :: cm1= cm1
	  real(dp) :: gmol_Afs=gmol_Afs
	  real(dp) :: kbar=kbar
  END TYPE ATOMIC_UNITS_TYPE

  TYPE(ATOMIC_UNITS_TYPE) :: au 

	TYPE(DICT_STRUCT), SAVE :: dict_atomic_units

	PRIVATE 
  PUBLIC :: atomic_units_initialize, au &
            ,convert_to_atomic_units &
            ,convert_to_unit &
            ,atomic_units_get_multiplier
CONTAINS

	
  subroutine atomic_units_initialize()
		IMPLICIT NONE

		call dict_atomic_units%init()
		call dict_atomic_units%store("1",1._dp)
		call dict_atomic_units%store("au",1._dp)		
		call dict_atomic_units%store("ev",au%eV)
		call dict_atomic_units%store("kcalpermol",au%kcalperMol)
		call dict_atomic_units%store("angstrom",au%bohr)
		call dict_atomic_units%store("bohr",1._dp/au%bohr)
		call dict_atomic_units%store("amu",1._dp/au%Mprot)
		call dict_atomic_units%store("femtoseconds",au%fs)
		call dict_atomic_units%store("fs",au%fs)
		call dict_atomic_units%store("picoseconds",au%fs/1000._dp)
		call dict_atomic_units%store("ps",au%fs/1000._dp)
		call dict_atomic_units%store("kelvin",au%kelvin)
		call dict_atomic_units%store("k",au%kelvin)
		call dict_atomic_units%store("thz",au%THz)
		call dict_atomic_units%store("tradhz",au%THz/(2*pi))
		call dict_atomic_units%store("cm-1",au%cm1)

	end subroutine atomic_units_initialize

	function convert_to_atomic_units(value,unit) RESULT(converted_val)
		IMPLICIT NONE
		REAL(dp), intent(in) :: value
		CHARACTER(*), intent(in) :: unit
		REAL(dp) :: converted_val
		REAL(dp) :: multiplier		

		multiplier=atomic_units_get_multiplier(unit)	
		converted_val=value/multiplier

	end function convert_to_atomic_units

	function convert_to_unit(value,unit) RESULT(converted_val)
		IMPLICIT NONE
		REAL(dp), intent(in) :: value
		CHARACTER(*), intent(in) :: unit
		REAL(dp) :: converted_val
		REAL(dp) :: multiplier		

		multiplier=atomic_units_get_multiplier(unit)	
		converted_val=value*multiplier

	end function convert_to_unit

	function atomic_units_get_multiplier(unit_string_in) result(multiplier)
		IMPLICIT NONE
		CHARACTER(*), intent(in) :: unit_string_in
		REAL(dp) :: multiplier

		REAL(dp) :: tmp_unit, tmp_power, tmp_power_read
		CHARACTER(:), ALLOCATABLE :: unit_string,unit_substring
		INTEGER :: i,unit_start,unit_stop,power_start,power_stop
		CHARACTER(1) :: current_char
		LOGICAL :: in_power

		unit_string=to_lower_case(trim(unit_string_in))

		multiplier=1._dp
		if(unit_string=="") RETURN

		unit_start=1
		unit_stop=0
		in_power=.FALSE.
		power_start=0
		power_stop=0
		tmp_power=1._dp
		do i=1,len(unit_string)

			current_char=unit_string(i:i)	
			!write(*,*) current_char
			SELECT CASE(current_char)
			CASE("^")

				if(in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif
				in_power=.TRUE.
				unit_stop=i-1
				if( unit_stop-unit_start < 0 ) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

			CASE("{")

				if(.not. in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				if(i+1 >= len(unit_string))	then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				power_start=i+1

			CASE("}")

				if(.not. in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				in_power=.FALSE.
				power_stop=i-1

				if(power_stop-power_start < 0) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				else
					READ(unit_string(power_start:power_stop),*) tmp_power_read
					tmp_power=tmp_power*tmp_power_read
				endif

				unit_substring=unit_string(unit_start:unit_stop)
				tmp_unit=unit_from_string(unit_substring)

				multiplier=multiplier*(tmp_unit**tmp_power)

				power_start=0
				power_stop=0
				unit_start=i+1
				unit_stop=0
			
			CASE('*')
				if(in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				if(unit_start==i) then
					unit_start=i+1
					tmp_power=1._dp
					CYCLE
				endif

				unit_stop=i-1
				if( unit_stop-unit_start < 0 ) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				unit_substring=unit_string(unit_start:unit_stop)
				tmp_unit=unit_from_string(unit_substring)
				multiplier=multiplier*(tmp_unit**tmp_power)

				unit_start=i+1
				unit_stop=0
				tmp_power=1._dp
			
			CASE('/')
				if(in_power) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				if(unit_start==i) then
					unit_start=i+1
					tmp_power=-1._dp
					CYCLE
				endif

				unit_stop=i-1
				if( unit_stop-unit_start < 0 ) then
					write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
					STOP "Execution stopped"
				endif

				unit_substring=unit_string(unit_start:unit_stop)
				tmp_unit=unit_from_string(unit_substring)
				multiplier=multiplier*(tmp_unit**tmp_power)

				unit_start=i+1
				unit_stop=0
				tmp_power=-1._dp

			CASE DEFAULT
				if(i+1 > len(unit_string)) then
					if(in_power) then
						write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
						STOP "Execution stopped"
					endif

					unit_stop=i
					if( unit_stop-unit_start < 0 ) then
						write(0,*) "Error: Syntax error in unit '"//unit_string//"' !"
						STOP "Execution stopped"
					endif

					unit_substring=unit_string(unit_start:unit_stop)
					tmp_unit=unit_from_string(unit_substring)
					multiplier=multiplier*(tmp_unit**tmp_power)
				endif
			END SELECT
		enddo

	end function atomic_units_get_multiplier

	function unit_from_string(unit_string) RESULT(unit)
		IMPLICIT NONE
		CHARACTER(*), intent(in) :: unit_string
		REAL(dp) :: unit

		! CLASS(DICT_DATA), pointer :: data

		! data => dict_get_key(dict_atomic_units, unit_string)
		! SELECT TYPE(data)
		! CLASS is (REAL_CONTAINER)
		! 	unit=data%value
		! CLASS DEFAULT
		! 	STOP "Error: type error in atomic_units dictionary!"
		! END SELECT
		unit=dict_atomic_units%get(unit_string)
	end function unit_from_string

end module atomic_units

