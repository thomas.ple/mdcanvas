module framework_initialization
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use trajectory_files, only: DEFAULT_TIME_COL &
								,DEFAULT_FORMATTED &
								,DEFAULT_STRIDE
	use atomic_units
	use string_operations
	use timer_module
  implicit none

CONTAINS

  subroutine initialize_framework(param_library,initialize_output)
	  use input_handler
	  !$ USE OMP_LIB
    implicit none
		TYPE(DICT_STRUCT), intent(inout) :: param_library
		LOGICAL, INTENT(in), optional :: initialize_output
	  REAL(dp) :: R, timeStart, timeFinish
	  CHARACTER(2) :: continue_arg
	  INTEGER :: i, nargs,u
    CHARACTER(:), allocatable :: input_file,abs_input
	  CHARACTER(:), allocatable :: n_proc
	  CHARACTER(:), allocatable :: current_arg
	  CHARACTER(:), allocatable :: output_dir 
		
		LOGICAL :: initialize_output_
	  LOGICAL :: n_proc_specified
	  LOGICAL :: continue_simulation
	  LOGICAL :: print_parameters
	  LOGICAL :: read_input_file

	  CHARACTER(:), allocatable :: command_line
	  CHARACTER (LEN = 256), allocatable :: fields(:)
    CHARACTER (LEN = 32), allocatable  :: field_types(:)
	  INTEGER :: nfields
    LOGICAL :: atomic_simulation
	  !$ INTEGER :: n_threads
	  TYPE(timer_type) :: timer
		CHARACTER(255) :: cwd

		if(present(initialize_output)) then
			initialize_output_ = initialize_output
		else
			initialize_output_ = .TRUE.
		endif
    
    !-------- RANDOM SEED --------
    	call set_random_seed()
    	!write(*,*)
    	!write(*,*) "****** TEST RANDOM SEED ******"
    	!do i=1,10
    	!	call random_number(R)
    	!	write(*,*) R
    	!enddo
    	!write(*,*) "******************************"
    	!write(*,*)
    	
    !-------- INITIALIZATION --------
    
    	call param_library%init()
    
    	call atomic_units_initialize()
    
    	nargs=command_argument_count()
    	!if(nargs <= 0) STOP "Error: at least the input file must be &
    	!	&provided as command argument."
    
    	call get_command(length=i)
    	allocate(CHARACTER(len=i) :: command_line)
    	call get_command(command_line)
    
    	allocate(fields(nargs+1),field_types(nargs+1))
    	CALL split_line( command_line, nfields, fields, field_types )
    
    	!if(trim(field_types(2)) /= STRING_TYPE) &
    	!	STOP "Error: first argument should be the input file!"
    	!input_file=trim(fields(2))
      
      read_input_file=.FALSE.
      n_proc_specified=.FALSE.
      continue_simulation=.FALSE.  
      print_parameters=.FALSE.
    	output_dir=""
      !PARSE COMMAND ARGUMENTS
      i=1
      DO 
    	  i=i+1
        if(i>nfields) EXIT
    		current_arg=trim(fields(i))

    		SELECT CASE(to_lower_case(current_arg))
    		CASE("-c")
    			continue_simulation=.TRUE.
    			write(*,*) "continue simulation"
    			CYCLE
        CASE("--outdir")
          if(output_dir /= "") STOP "Error: outdir specified multiple times!"
          if((i+1)>nfields) STOP "Error: could not read outdir!"
          output_dir=trim(fields(i+1))
          i=i+1
          CYCLE

        CASE("--in")
    		  if(read_input_file) STOP "Error: input file specified multiple times!"
          read_input_file=.TRUE.
          if((i+1)>nfields) STOP "Error: could not read input file!"
          input_file=trim(fields(i+1))
          i=i+1
          CYCLE
        CASE("--iproc")
          if(n_proc_specified) STOP "Error: proc index specified multiple times!"
          n_proc_specified=.TRUE.
          if((i+1)>nfields) STOP "Error: could not read proc index!"
          n_proc=trim(fields(i+1))
    			!write(*,*) "proc "//n_proc
          i=i+1
          CYCLE
        CASE("--print-parameters","-p")
    			print_parameters=.TRUE.
    			CYCLE
    		END SELECT
    
    		write(0,*) "Warning: unrecognized argument: "//current_arg
    	ENDDO
    
    	!write(*,*)
    	!write(*,*) "### Initialization ###"
    
    	! PARSE INPUT FILE AND FILL LIBRARY
    	if(read_input_file) call parse_input_file(input_file,param_library)
    	!STORE continue_simulation
    	if(.NOT. param_library%has_key("PARAMETERS")) call param_library%add_child("PARAMETERS")
      call param_library%store("PARAMETERS/continue_simulation",continue_simulation)
      
      if(trim(output_dir) /= "") &
        call param_library%store("PARAMETERS/output_dir",trim(output_dir))
    	!GET OPENMP NUMBER OF THREADS
    	!$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
    	!$ write(*,*) "OpenMP max number of threads=",n_threads
    	!$ call omp_set_num_threads(n_threads)
    
    	!INITIALIZE OUTPUT
			if(initialize_output_) then
				if(n_proc_specified) then
					call initialize_general_output(param_library,n_proc)
				else
					call initialize_general_output(param_library)
				endif

		    CALL GETCWD(cwd)
		    abs_input=trim(input_file)
		    if(abs_input(1:1)/="/") &
		    	abs_input=trim(cwd)//"/"//abs_input
		    
		    write(*,*) abs_input
        call system("cp "//abs_input//" "//output%working_directory//"/input_data.in")
      endif 	
      
      if(print_parameters) call param_library%print()		
  end subroutine initialize_framework

	subroutine initialize_general_output(param_library,n_proc)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		CHARACTER(*), intent(in), optional :: n_proc
		CHARACTER(:), allocatable :: outdir,file_dir, engine
		LOGICAL :: file_exist
		
    outdir=param_library%get("PARAMETERS/output_dir",default="./")
    !! @input_file PARAMETERS/output_dir (main)
    
    outdir=trim(outdir)
		if(present(n_proc)) then
			file_dir=param_library%get("PARAMETERS/file_dir",default="proc")
			!! @input_file PARAMETERS/file_dir (main, default="proc", if present(n_proc))
			file_dir=trim(file_dir)//"_"//trim(n_proc)
		else
			file_dir=""
		endif

		INQUIRE(FILE=outdir//"/EXIT",EXIST=file_exist)
		IF(file_exist) &
			call system("rm "//outdir//"/EXIT")
		call output%set_working_directory(outdir,file_dir)

		if(param_library%has_key("OUTPUT")) then
		  DEFAULT_FORMATTED=param_library%get("OUTPUT/formatted_by_default",default=DEFAULT_FORMATTED)
		  !! @input_file OUTPUT/formatted_by_default (main, default=DEFAULT_FORMATTED)
		  DEFAULT_TIME_COL=param_library%get("OUTPUT/time_column_by_default",default=DEFAULT_TIME_COL)
		  !! @input_file OUTPUT/time_column_by_default (main, default=DEFAULT_TIME_COL)
		  DEFAULT_STRIDE=param_library%get("OUTPUT/stride_by_default",default=DEFAULT_STRIDE)
		  !! @input_file OUTPUT/stride_by_default (main, default=DEFAULT_STRIDE)
    endif
	end subroutine initialize_general_output

  subroutine open_output_files(param_library)
    implicit none
		TYPE(DICT_STRUCT), intent(in) :: param_library
    integer :: u
    LOGICAL :: continue_simulation

    if(param_library%has_key("OUTPUT")) then
	    continue_simulation=param_library%get("PARAMETERS/continue_simulation",default=.FALSE.)
      call output%open_files(append=continue_simulation)
	  	call output%write_description_file(clear=.TRUE.)

	  	OPEN(newunit=u,file=output%working_directory//"/input_data.in")
	  		CALL param_library%print(unit=u)
	  	CLOSE(u)
	  endif
  end subroutine open_output_files

end module framework_initialization
