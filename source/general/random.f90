MODULE random
!! handling of random numbers
	use kinds, only: dp
	IMPLICIT NONE

CONTAINS

	subroutine RandGauss(R)
	!! generate a gaussian random number (box-muller)
		implicit none
		real(dp), intent(out) :: R
		real(dp) :: u1,u2,w,rnd(2)	

		do
			call random_number(rnd)
			u1=2*rnd(1)-1
			u2=2*rnd(2)-1
			w=u1*u1+u2*u2
			if (w<1) exit
		enddo
		w=sqrt(-2*log(w)/w)
		R=u1*w
		
	end subroutine RandGauss

	subroutine RandGaussN(R)
	!! fill an array with gaussian random number (box-muller)
		implicit none
		real(dp), intent(inout) :: R(:)
		real(dp) :: u1,u2,w,rnd(2)
		integer :: p,rest,i,n
		
		n=size(R)
		p=n/2
		rest=mod(n,2)
		do i=1,2*p-1,2
			do
				call random_number(rnd)
				u1=2*rnd(1)-1
				u2=2*rnd(2)-1
				w=u1*u1+u2*u2
				if (w<1) exit
			enddo
			w=sqrt(-2*log(w)/w)
			R(i)=u1*w
			R(i+1)=u2*w
		enddo
		if(rest/=0) call RandGauss(R(n))
		
	end subroutine RandGaussN

	subroutine set_random_seed()
	!! set a random seed using system_clock
		implicit none
		integer :: ns, k
		integer, dimension(:), allocatable :: s ! seed setup array

		call random_seed(size=ns) ; allocate(s(ns))    ! set seed for random numbers
		do k = 1, ns ; call system_clock(s(k)) ; enddo
		call random_seed(put=s) ; deallocate(s)
	end subroutine set_random_seed

END MODULE random
