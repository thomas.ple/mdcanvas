MODULE string_operations
    USE kinds
!! collection of operations on strings
    IMPLICIT NONE

    INTEGER, PARAMETER :: MAX_FIELDS=10000

    ! TYPE STRING_WRAPPER
    !     CHARACTER(:), ALLOCATABLE :: val
    ! END TYPE

    INTERFACE ASSIGNMENT(=)        
        MODULE PROCEDURE str_to_real
        MODULE PROCEDURE str_to_int
        MODULE PROCEDURE str_to_int_array
        MODULE PROCEDURE str_to_real_array
        MODULE PROCEDURE str_to_logical
        MODULE PROCEDURE str_to_logical_array
     !   MODULE PROCEDURE str_wrap_to_str
    END INTERFACE

    ! PRIVATE
    ! PUBLIC :: to_upper_case, to_lower_case &
    !         ,string_to_logical ,ASSIGNMENT(=), STRING_WRAPPER

CONTAINS

    FUNCTION to_upper_case( str ) RESULT( string )
    !! @brief Changes a string to upper case

        IMPLICIT NONE

        CHARACTER(LEN = *), INTENT(IN) :: str
        CHARACTER(LEN = LEN(str))      :: string

    ! Localvariables
        INTEGER :: ic
        INTEGER :: i
        CHARACTER(26), PARAMETER :: cap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        CHARACTER(26), PARAMETER :: low = 'abcdefghijklmnopqrstuvwxyz'

    ! Capitalize each letter if it is lowecase
        string = str
        DO i = 1, LEN_TRIM(str)
        ic = INDEX(low, str(i:i))
        IF (ic > 0) string(i:i) = cap(ic:ic)
        END DO

    END FUNCTION to_upper_case

    !============================================================================

    FUNCTION to_lower_case( str ) RESULT( string )
    !! @brief Changes a string to lower case

        IMPLICIT NONE

        CHARACTER(LEN = *), INTENT(IN) :: str
        CHARACTER(LEN = LEN(str))      :: string

    !! Local variables
        INTEGER :: ic
        INTEGER :: i
        CHARACTER(26), PARAMETER :: cap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        CHARACTER(26), PARAMETER :: low = 'abcdefghijklmnopqrstuvwxyz'

    !! Lower each letter if it is a capital letter
        string = str
        DO i = 1, LEN_TRIM(str)
        ic = INDEX(cap, str(i:i))
        IF (ic > 0) string(i:i) = low(ic:ic)
        END DO

    END FUNCTION to_lower_case

!------------------------------------------------

SUBROUTINE split_line(inputline, nfields, fields, field_types, &
       separator)
    
    !! @brief This routine takes a line and splits it into fields, using
    !! either an optional separator character or by default a whitespace
    !! character: The results are the number of fields found, their values
    !! and their types. Note on the types: They are generalised, so that
    !! for example '5' (without the quotation marks; if they are provided,
    !! the type is automatically a "STRING") is interpreted to be an
    !! integer number, but when checking the value, it could also be a real
    !! number, or even a string, without any numerical meaning.
    !! (TAKEN FROM PaPIM)

    IMPLICIT NONE
    
    ! Arguments
    CHARACTER (LEN = *), INTENT (IN) :: inputline
    INTEGER, INTENT (OUT) :: nfields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (OUT) :: fields
    CHARACTER (LEN = *), DIMENSION (:), INTENT (OUT) :: field_types
    CHARACTER, OPTIONAL, INTENT (IN) :: separator
    
    ! Local
    CHARACTER(*), PARAMETER :: separators=" ,="//char(9), comment_chars="#!"
    CHARACTER :: separator_in_use
    INTEGER :: i, nth_character_argument, field, error_code
    LOGICAL :: in_argument
    LOGICAL :: in_string_single, in_string_double
    
    INTEGER :: test_integer
    REAL :: test_real
    
    !--------------------------------------------------------------------------
    
    IF (PRESENT (separator)) THEN
       separator_in_use = separator
    ELSE
       separator_in_use = " "
    END IF
    
    fields(:) = ""
    field_types(:) = "NONE"
    
    nfields = 0
    nth_character_argument = 0
    in_string_single = .FALSE.
    in_string_double = .FALSE.
    in_argument = .FALSE.
    DO i = 1, LEN(TRIM(inputline))
       IF (inputline(i:i) == "'") THEN
          IF (in_string_double) THEN
             nth_character_argument = nth_character_argument + 1
             fields(nfields)(nth_character_argument:nth_character_argument) &
                  = inputline(i:i)
          ELSE
             IF (in_string_single) THEN
                ! End string
                in_string_single = .FALSE.
             ELSE
                ! Start string
                in_string_single = .TRUE.
                nfields = nfields + 1
                IF (nfields > SIZE(fields)) THEN
                   WRITE(6,*) "Too many arguments"
                   ! CALL error(...)
                END IF
                field_types(nfields) = "STRING"
                nth_character_argument = 0
             END IF
             !nth_character_argument = nth_character_argument + 1
             !fields(nfields)(nth_character_argument:nth_character_argument) &
             !     = inputline(i:i)
          END IF
       ELSE IF (inputline(i:i) == '"') THEN
          IF (in_string_single) THEN
             nth_character_argument = nth_character_argument + 1
             fields(nfields)(nth_character_argument:nth_character_argument) &
                  = inputline(i:i)
          ELSE
             IF (in_string_double) THEN
                ! End string
                in_string_double = .FALSE.
             ELSE
                ! Start string
                in_string_double = .TRUE.
                nfields = nfields + 1
                IF (nfields > SIZE(fields)) THEN
                   WRITE(6,*) "Too many arguments"
                   ! CALL error(...)
                END IF
                field_types(nfields) = "STRING"
                nth_character_argument = 0
             END IF
             !nth_character_argument = nth_character_argument + 1
             !fields(nfields)(nth_character_argument:nth_character_argument) &
             !     = inputline(i:i)
          END IF
       ELSE IF (in_string_single .OR. in_string_double) THEN
          nth_character_argument = nth_character_argument + 1
          fields(nfields)(nth_character_argument:nth_character_argument) &
               = inputline(i:i)
       ELSE IF (INDEX(separators,inputline(i:i))/=0) THEN
          IF (in_argument) THEN
             in_argument = .FALSE.
          END IF
       ELSE
          IF ( INDEX(comment_chars,inputline(i:i))/=0 .AND. .NOT. in_argument) THEN
             EXIT
          END IF
          IF (.NOT. in_argument) THEN
             nfields = nfields + 1
             IF (nfields > SIZE(fields)) THEN
                WRITE(6,*) "Too many arguments"
                ! CALL error(...)
             END IF
             nth_character_argument = 0
             in_argument = .TRUE.
          END IF
          nth_character_argument = nth_character_argument + 1
          fields(nfields)(nth_character_argument:nth_character_argument) &
               = inputline(i:i)
       END IF
    END DO
    
    IF (in_string_single .OR. in_string_double) THEN
       write(6,*) "String not closed"
       ! CALL error(...)
    END IF
    
    ! #1 Test for logical
    ! #2 Test for real
    ! #3 Test for integer
    ! #4 else: string
    DO field = 1, nfields
       IF (field_types(field) /= "NONE") CYCLE
       IF (to_upper_case(fields(field)) == "TRUE"     .OR. &
            to_upper_case(fields(field)) == "YES"     .OR. &
            to_upper_case(fields(field)) == ".TRUE."  .OR. &
            to_upper_case(fields(field)) == "NO"      .OR. &
            to_upper_case(fields(field)) == "FALSE"   .OR. &
            to_upper_case(fields(field)) == ".FALSE.") THEN
          field_types(field) = "LOGICAL"
          CYCLE
       END IF
       IF (INDEX(fields(field), '.') <= 0) THEN
          READ(UNIT = fields(field), FMT = *, IOSTAT = error_code) test_integer
          IF (error_code == 0) THEN
              field_types(field) = "INTEGER"
              CYCLE
          END IF
       ELSE
          READ(UNIT = fields(field), FMT = *, IOSTAT = error_code) test_real
          IF (error_code == 0) THEN
              field_types(field) = "REAL"
              CYCLE
          END IF
       ENDIF
       field_types(field) = "STRING"
    END DO
    
  END SUBROUTINE split_line

  function string_to_logical(string) result(logi)
  !! @brief converts a string to the boolean it represents
  !! accepted representations are:
  !!    - TRUE, YES, .TRUE.
  !!    - FALSE, NO, .FALSE.
  !! the function is case insensitive
		IMPLICIT NONE
		CHARACTER(*), INTENT(in) :: string
		LOGICAL :: logi

		SELECT CASE(to_upper_case(trim(string)))
		CASE("TRUE","YES",".TRUE.")
			logi=.TRUE.
		CASE("FALSE","NO",".FALSE.")
			logi=.FALSE.
		CASE DEFAULT
			write(0,*) "Error: Unknown string representation for a logical : "//to_upper_case(trim(string))
			STOP "Execution stopped."
		END SELECT

	end function string_to_logical

    function int_to_str(value) result(str)
        IMPLICIT NONE
        INTEGER, INTENT(in) :: value
        CHARACTER(:), ALLOCATABLE :: str
        INTEGER :: n_char
        CHARACTER(10) :: n_char_char

        if(value==0) then
            n_char=1
        else
            n_char=int(log10(real(value)))+1
        endif
        !write(0,*) n_char
        allocate(character(n_char) :: str)
        write(n_char_char,'(i10.10)') n_char
       ! write(0,*) n_char_char
        write(str,'(i'//trim(n_char_char)//')') value
    end function int_to_str

! !------------------------------------------------
! ! assignment subroutines

    SUBROUTINE str_to_int(value,string)
        IMPLICIT NONE
        INTEGER, INTENT(INOUT) :: value
        CHARACTER(*), INTENT(IN) :: string
        INTEGER :: nfields
        CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

        CALL split_line( string, nfields, fields, field_types )
        if(nfields/=1 .OR. field_types(1)/=int_type ) then
            write(0,*) "Error: '"//string//"' does not represent "//int_type
            STOP "Execution stopped."
        endif        
        read(fields(1),*) value
    END SUBROUTINE 

    SUBROUTINE str_to_real(value,string)
        IMPLICIT NONE
        REAL(dp), INTENT(INOUT) :: value
        CHARACTER(*), INTENT(IN) :: string
        INTEGER :: nfields
        CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

        CALL split_line( string, nfields, fields, field_types )
        if(nfields/=1 .OR. field_types(1)/=real_type ) then
            write(0,*) "Error: '"//string//"' does not represent "//real_type
            STOP "Execution stopped."
        endif        
        read(fields(1),*) value
    END SUBROUTINE 

    SUBROUTINE str_to_int_array(value,string)
        IMPLICIT NONE
        INTEGER, ALLOCATABLE, INTENT(INOUT) :: value(:)
        CHARACTER(*), INTENT(IN) :: string
        INTEGER :: nfields,i
        CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

        CALL split_line( string, nfields, fields, field_types )
        if(nfields<1) then
            write(0,*) "Error: '"//string//"' does not represent "//int_array_type
            STOP "Execution stopped."
        endif 
        if(allocated(value)) deallocate(value)         
        allocate(value(nfields)) 
        DO i=1,nfields
            if(field_types(i)==int_type) then
                read(fields(i),*) value(i)
            else
                write(0,*) "Error: '"//string//"' does not represent "//real_array_type
                STOP "Execution stopped."
            endif
        ENDDO
    END SUBROUTINE 

    SUBROUTINE str_to_real_array(value,string)
        IMPLICIT NONE
        REAL(dp), ALLOCATABLE, INTENT(INOUT) :: value(:)
        CHARACTER(*), INTENT(IN) :: string
        INTEGER :: nfields,i
        CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

        CALL split_line( string, nfields, fields, field_types )
        if(nfields<1) then
            write(0,*) "Error: '"//string//"' does not represent "//real_array_type
            STOP "Execution stopped."
        endif
        if(allocated(value)) deallocate(value)     
        allocate(value(nfields)) 
        DO i=1,nfields
            if(field_types(i)==real_type .OR. field_types(i)==int_type) then
                read(fields(i),*) value(i)
            else
                write(0,*) "Error: '"//string//"' does not represent "//real_array_type
                STOP "Execution stopped."
            endif
        ENDDO
    END SUBROUTINE 

    SUBROUTINE str_to_logical(value,string)
        IMPLICIT NONE
        LOGICAL, INTENT(INOUT) :: value
        CHARACTER(*), INTENT(IN) :: string
        INTEGER :: nfields
        CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

        CALL split_line( string, nfields, fields, field_types )
        if(nfields/=1 .OR. field_types(1)/=logical_type ) then
            write(0,*) "Error: '"//string//"' does not represent "//logical_type
            STOP "Execution stopped."
        endif        
        value=string_to_logical(fields(1))
    END SUBROUTINE 

    SUBROUTINE str_to_logical_array(value,string)
        IMPLICIT NONE
        LOGICAL, ALLOCATABLE, INTENT(INOUT) :: value(:)
        CHARACTER(*), INTENT(IN) :: string
        INTEGER :: nfields,i
        CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

        CALL split_line( string, nfields, fields, field_types )
        if(nfields<1 .OR. any(field_types/=logical_type)) then
            write(0,*) "Error: '"//string//"' does not represent "//logical_array_type
            STOP "Execution stopped."
        endif    
        if(allocated(value)) deallocate(value)   
        allocate(value(nfields)) 
        DO i=1,nfields
            value(i)=string_to_logical(fields(i))
        ENDDO
    END SUBROUTINE

!------------------------------------------------
! assignment subroutines

    ! SUBROUTINE str_to_int(value,string)
    !     IMPLICIT NONE
    !     INTEGER, INTENT(OUT) :: value
    !     TYPE(STRING_WRAPPER), INTENT(in) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string%val, nfields, fields, field_types )
    !     if(nfields/=1 .OR. field_types(1)/=int_type ) then
    !         write(0,*) "Error: '"//string%val//"' does not represent "//int_type
    !         STOP "Execution stopped."
    !     endif        
    !     read(fields(1),*) value
    ! END SUBROUTINE str_to_int
    ! SUBROUTINE str_to_int(value,string)
    !     IMPLICIT NONE
    !     INTEGER, INTENT(INOUT) :: value
    !     CHARACTER(*), INTENT(IN) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string, nfields, fields, field_types )
    !     if(nfields/=1 .OR. field_types(1)/=int_type ) then
    !         write(0,*) "Error: '"//string//"' does not represent "//int_type
    !         STOP "Execution stopped."
    !     endif        
    !     read(fields(1),*) value
    ! END SUBROUTINE 

    ! SUBROUTINE str_to_real(value,string)
    !     IMPLICIT NONE
    !     REAL(dp), INTENT(INOUT) :: value
    !     CHARACTER(*), INTENT(IN) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string, nfields, fields, field_types )
    !     if(nfields/=1 .OR. field_types(1)/=real_type ) then
    !         write(0,*) "Error: '"//string//"' does not represent "//real_type
    !         STOP "Execution stopped."
    !     endif        
    !     read(fields(1),*) value
    ! END SUBROUTINE 

    ! SUBROUTINE str_to_int_array(value,string)
    !     IMPLICIT NONE
    !     INTEGER, ALLOCATABLE, INTENT(INOUT) :: value(:)
    !     CHARACTER(*), INTENT(IN) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string, nfields, fields, field_types )
    !     if(nfields<1 .OR. any(field_types/=int_type)) then
    !         write(0,*) "Error: '"//string//"' does not represent "//int_array_type
    !         STOP "Execution stopped."
    !     endif       
    !     allocate(value(nfields)) 
    !     read(fields(:),*) value(:)
    ! END SUBROUTINE 

    ! SUBROUTINE str_to_real_array(value,string)
    !     IMPLICIT NONE
    !     REAL(dp), ALLOCATABLE, INTENT(INOUT) :: value(:)
    !     CHARACTER(*), INTENT(IN) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string, nfields, fields, field_types )
    !     if(nfields<1 .OR. any(field_types/=real_type .and. field_types/=int_type)) then
    !         write(0,*) "Error: '"//string//"' does not represent "//real_array_type
    !         STOP "Execution stopped."
    !     endif       
    !     allocate(value(nfields)) 
    !     read(fields(:),*) value(:)
    ! END SUBROUTINE 

    ! SUBROUTINE str_to_logical(value,string)
    !     IMPLICIT NONE
    !     LOGICAL, INTENT(INOUT) :: value
    !     CHARACTER(*), INTENT(IN) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string, nfields, fields, field_types )
    !     if(nfields/=1 .OR. field_types(1)/=logical_type ) then
    !         write(0,*) "Error: '"//string//"' does not represent "//logical_type
    !         STOP "Execution stopped."
    !     endif        
    !     value=string_to_logical(fields(1))
    ! END SUBROUTINE 

    ! SUBROUTINE str_to_logical_array(value,string)
    !     IMPLICIT NONE
    !     LOGICAL, ALLOCATABLE, INTENT(INOUT) :: value(:)
    !     CHARACTER(*), INTENT(IN) :: string
    !     INTEGER :: nfields,i
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string, nfields, fields, field_types )
    !     if(nfields<1 .OR. any(field_types/=logical_type)) then
    !         write(0,*) "Error: '"//string//"' does not represent "//logical_array_type
    !         STOP "Execution stopped."
    !     endif    
    !     allocate(value(nfields)) 
    !     DO i=1,nfields
    !         value(i)=string_to_logical(fields(i))
    !     ENDDO
    ! END SUBROUTINE
    ! SUBROUTINE str_to_real(value,string)
    !     IMPLICIT NONE
    !     REAL(dp), INTENT(OUT) :: value
    !     TYPE(STRING_WRAPPER), INTENT(in) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string%val, nfields, fields, field_types )
    !     if(nfields/=1 .OR. field_types(1)/=real_type ) then
    !         write(0,*) "Error: '"//string%val//"' does not represent "//real_type
    !         STOP "Execution stopped."
    !     endif        
    !     read(fields(1),*) value
    ! END SUBROUTINE str_to_real

    ! SUBROUTINE str_to_int_array(value,string)
    !     IMPLICIT NONE
    !     INTEGER, ALLOCATABLE, INTENT(OUT) :: value(:)
    !     TYPE(STRING_WRAPPER), INTENT(in) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string%val, nfields, fields, field_types )
    !     if(nfields<1 .OR. any(field_types/=int_type)) then
    !         write(0,*) "Error: '"//string%val//"' does not represent "//int_array_type
    !         STOP "Execution stopped."
    !     endif       
    !     allocate(value(nfields)) 
    !     read(fields(:),*) value(:)
    ! END SUBROUTINE str_to_int_array

    ! SUBROUTINE str_to_real_array(value,string)
    !     IMPLICIT NONE
    !     REAL(dp), ALLOCATABLE, INTENT(OUT) :: value(:)
    !     TYPE(STRING_WRAPPER), INTENT(in) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string%val, nfields, fields, field_types )
    !     if(nfields<1 .OR. any(field_types/=real_type .and. field_types/=int_type)) then
    !         write(0,*) "Error: '"//string%val//"' does not represent "//real_array_type
    !         STOP "Execution stopped."
    !     endif       
    !     allocate(value(nfields)) 
    !     read(fields(:),*) value(:)
    ! END SUBROUTINE str_to_real_array

    ! SUBROUTINE str_to_logical(value,string)
    !     IMPLICIT NONE
    !     LOGICAL, INTENT(OUT) :: value
    !     TYPE(STRING_WRAPPER), INTENT(in) :: string
    !     INTEGER :: nfields
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string%val, nfields, fields, field_types )
    !     if(nfields/=1 .OR. field_types(1)/=logical_type ) then
    !         write(0,*) "Error: '"//string%val//"' does not represent "//logical_type
    !         STOP "Execution stopped."
    !     endif        
    !     value=string_to_logical(fields(1))
    ! END SUBROUTINE str_to_logical

    ! SUBROUTINE str_to_logical_array(value,string)
    !     IMPLICIT NONE
    !     LOGICAL, ALLOCATABLE, INTENT(OUT) :: value(:)
    !     TYPE(STRING_WRAPPER), INTENT(in) :: string
    !     INTEGER :: nfields,i
    !     CHARACTER (LEN = 256), DIMENSION(MAX_FIELDS) :: fields
    ! 	CHARACTER (LEN = 32), DIMENSION(MAX_FIELDS)  :: field_types

    !     CALL split_line( string%val, nfields, fields, field_types )
    !     if(nfields<1 .OR. any(field_types/=logical_type)) then
    !         write(0,*) "Error: '"//string%val//"' does not represent "//logical_array_type
    !         STOP "Execution stopped."
    !     endif    
    !     allocate(value(nfields)) 
    !     DO i=1,nfields
    !         value(i)=string_to_logical(fields(i))
    !     ENDDO
    ! END SUBROUTINE str_to_logical_array

    ! SUBROUTINE str_wrap_to_str(value,string)
    !     IMPLICIT NONE
    !     CHARACTER(:), ALLOCATABLE, INTENT(OUT) :: value
    !     TYPE(STRING_WRAPPER), INTENT(in) :: string

    !     value=string%val
    ! END SUBROUTINE str_wrap_to_str

END MODULE string_operations
