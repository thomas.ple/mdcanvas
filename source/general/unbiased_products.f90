MODULE unbiased_products
	USE kinds, only: dp
	IMPLICIT NONE
	
	INTERFACE unbiased_product
		MODULE PROCEDURE unbiased_product_3nx1
		MODULE PROCEDURE unbiased_product_2nx1
	END INTERFACE

	PRIVATE
	PUBLIC :: unbiased_product, test_unbiased_product

CONTAINS

	function unbiased_product_3nx1(a,b,c) result(prod)
        IMPLICIT NONE
        REAL(dp), DIMENSION(:), INTENT(IN) :: a,b,c
        REAL(dp) :: prod,bb(size(b)),cc(size(c))
        INTEGER :: n,i,j,k

        n=size(a)
        prod=0
        if(n/=size(b)) STOP "Error: a and b must be the same length for unbiased product!"
        if(n/=size(c)) STOP "Error: a, b and c must be the same length for unbiased product!"
        bb=b
        cc=c
        if(n<3) STOP "Error: 3 samples must be present for unbiased product of 3 terms!"
        DO i=1,n            
            cc=CSHIFT(cc,1)
            bb=CSHIFT(bb,1)
            prod = prod+ a(i)*unbiased_product_2nx1(bb(1:n-1),cc(1:n-1))
        ENDDO
       ! write(*,*) "count=",count,(n*(n*n-3*n+2))
        prod = prod / real(n,dp)

    end function unbiased_product_3nx1
    
    function unbiased_product_3nx1_bis(a,b,c) result(prod)
        IMPLICIT NONE
        REAL(dp), DIMENSION(:), INTENT(IN) :: a,b,c
        REAL(dp) :: prod,bb(size(b)),cc(size(c))
        INTEGER(8) :: n,i,j,k,count
        
        n=size(a)
        prod=0
        if(n/=size(b)) STOP "Error: a and b must be the same length for unbiased product!"
        if(n/=size(c)) STOP "Error: a, b and c must be the same length for unbiased product!"
        bb=b
        cc=c
        count=0
        if(n<3) STOP "Error: 3 samples must be present for unbiased product of 3 terms!"
        DO i=1,n            
             DO j=1,n 
                 if(i==j) CYCLE
                 DO k=1,n
                     if(i==k .or. j==k) CYCLE
                     prod=prod+a(i)*b(j)*c(k)
                     count=count+1
                 ENDDO
             ENDDO
        ENDDO
       	write(*,*) "count=",count,(n*(n*n-3*n+2))
        prod = prod / count

    end function unbiased_product_3nx1_bis

    function unbiased_product_2nx1(a,b) result(prod)
        IMPLICIT NONE
        REAL(dp), DIMENSION(:), INTENT(IN) :: a,b
        REAL(dp) :: prod
        INTEGER :: n,count,i,j,k

        n=size(a)
        count=0
        prod=0
        if(n/=size(b)) STOP "Error: a and b must be the same length for unbiased product!"
        DO i=1,n-1 
            DO j=i+1,n 
                prod=prod+a(i)*b(j)+a(j)*b(i)
            ENDDO
        ENDDO
        prod = prod / real(n*n-n,dp)

    end function unbiased_product_2nx1
    
    subroutine test_unbiased_product()
    	IMPLICIT NONE
    	REAL(dp), ALLOCATABLE :: a(:),b(:),c(:),prod_mat(:,:)
    	REAL(dp) :: prod,prod_ref
    	INTEGER :: i,j,n
    	REAL :: t0,tf,t,t_ref
    	
    	!TEST 2nx1
    	ALLOCATE(a(4),b(4))
    	call random_number(a)
    	call random_number(b)
    	prod=unbiased_product(a,b)
    	prod_ref=(   a(1)*b(2) &
    				+a(1)*b(3) &
    				+a(1)*b(4) &
    				+a(2)*b(1) &
    				+a(2)*b(3) &
    				+a(2)*b(4) &
    				+a(3)*b(1) &
    				+a(3)*b(2) &
    				+a(3)*b(4) &
    				+a(4)*b(1) &
    				+a(4)*b(2) &
    				+a(4)*b(3) &    	
    		) / 12._dp
    	write(*,*) "test 1:",prod,prod_ref
    	
    	DEALLOCATE(a,b)
    	n=10000
    	ALLOCATE(a(n),b(n),c(n),prod_mat(n,n))
    	call random_number(a)
    	call random_number(b)
    	call cpu_time(t0)
    	prod=unbiased_product(a,b)
    	call cpu_time(tf)
    	t=tf-t0
    	call cpu_time(t0)
    	forall (i=1:n) 
		  forall(j=1:n) prod_mat(i,j) = a(i)*b(j)
		  c(i)=a(i)*b(i)
		end forall 
		prod_ref=(SUM(prod_mat)-SUM(c))/real(n*n-n,dp)
		call cpu_time(tf)
		t_ref=tf-t0
		write(*,*) "test 2:",prod,prod_ref
		write(*,*) "       time:",t,"s    ",t_ref,"s"
		
		DEALLOCATE(a,b,c)
    	ALLOCATE(a(3),b(3),c(3))
    	call random_number(a)
    	call random_number(b)
    	call random_number(c)
    	prod=unbiased_product(a,b,c)
    	prod_ref=(   a(1)*b(2)*c(3) &
    				+a(1)*b(3)*c(2) &
    				+a(2)*b(1)*c(3) &
    				+a(2)*b(3)*c(1) &
    				+a(3)*b(1)*c(2) &
    				+a(3)*b(2)*c(1) &
    			) / 6._dp
		write(*,*) "test 3:",prod,prod_ref	 
		
	
		DEALLOCATE(a,b,c)
    	n=1000
    	ALLOCATE(a(n),b(n),c(n))
    	call random_number(a)
    	call random_number(b)   
    	call random_number(c) 	
    	call cpu_time(t0)
    	prod=unbiased_product(a,b,c)
    	call cpu_time(tf)
    	t=tf-t0
    	call cpu_time(t0)
    	prod_ref=unbiased_product_3nx1_bis(a,b,c)
    	call cpu_time(tf)
    	t_ref=tf-t0
    	write(*,*) "test 4:",prod,prod_ref
		write(*,*) "       time:",t,"s    ",t_ref,"s"
    
    end subroutine test_unbiased_product
	
END MODULE unbiased_products
