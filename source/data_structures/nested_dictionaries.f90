MODULE nested_dictionaries
    USE kinds
    USE linkedlist
    USE string_operations
    IMPLICIT NONE


    INTEGER, PARAMETER :: DICT_KEY_LENGTH=40
    INTEGER, PARAMETER :: HASH_SIZE  = 100
    INTEGER, PARAMETER :: MULTIPLIER = 31
    CHARACTER(1), PARAMETER :: PATH_SEP = "/"
    LOGICAL, PARAMETER :: CASE_INSENSITIVE=.TRUE.

    type, extends(LIST_DATA) :: HASH_DATA
        character(len=DICT_KEY_LENGTH) :: key
        CHARACTER(:), ALLOCATABLE      :: value
        class(DICT_STRUCT), pointer    :: sub_dict => null()
        LOGICAL :: is_leaf=.TRUE.
    CONTAINS
        PROCEDURE :: destroy => hash_data_destroy
    end type

    type HASH_TANK
        class(LINKED_LIST), pointer :: list
    end type HASH_TANK

    type DICT_STRUCT
        private
        type(HASH_TANK), pointer, dimension(:) :: table => null()
        INTEGER :: num_of_entries=-1
        class(DICT_STRUCT), pointer    :: parent_dict => null()
    CONTAINS
        PROCEDURE :: init => dict_init
        PROCEDURE :: destroy => dict_destroy
       ! PROCEDURE :: has_key => dict_has_key
        PROCEDURE :: has_key => dict_has_key_rec
        PROCEDURE :: add_sub_dict => dict_add_sub_dict
		PROCEDURE :: add_child => dict_add_sub_dict
        PROCEDURE :: delete => dict_delete_key
		PROCEDURE, PRIVATE :: get_value => dict_get_value_rec
        PROCEDURE, PRIVATE :: get_value_rec => dict_get_value_rec
        PROCEDURE :: get_sub_dict => dict_get_sub_dict
		PROCEDURE :: get_child => dict_get_sub_dict
        PROCEDURE, PRIVATE :: add_entry => dict_add_entry_rec
        PROCEDURE, PRIVATE :: get_elem => dict_get_elem
        PROCEDURE, PRIVATE :: get_entry => dict_get_entry
        PROCEDURE, PRIVATE :: get_entry_rec => dict_get_entry_rec
        PROCEDURE :: print => dict_print_rec

        PROCEDURE, PRIVATE :: add_value_string => dict_add_value_rec
        PROCEDURE, PRIVATE :: add_value_int => dict_add_value_int
        PROCEDURE, PRIVATE :: add_value_int_array => dict_add_value_int_array
        PROCEDURE, PRIVATE :: add_value_real => dict_add_value_real
        PROCEDURE, PRIVATE :: add_value_real_array => dict_add_value_real_array
        PROCEDURE, PRIVATE :: add_value_float => dict_add_value_float
        PROCEDURE, PRIVATE :: add_value_float_array => dict_add_value_float_array
        PROCEDURE, PRIVATE :: add_value_logical => dict_add_value_logical
        PROCEDURE, PRIVATE :: add_value_logical_array => dict_add_value_logical_array
        GENERIC :: store => add_value_string &
                                , add_value_int &
                                , add_value_int_array &
                                , add_value_real &
                                , add_value_real_array &
                                , add_value_float &
                                , add_value_float_array &
                                , add_value_logical &
                                , add_value_logical_array

        PROCEDURE, PRIVATE :: get_value_str_wrap => dict_get_value_rec
        PROCEDURE, PRIVATE :: get_value_int => dict_get_value_int
        PROCEDURE, PRIVATE :: get_value_int_array => dict_get_value_int_array
        PROCEDURE, PRIVATE :: get_value_real => dict_get_value_real
        PROCEDURE, PRIVATE :: get_value_float => dict_get_value_float
        PROCEDURE, PRIVATE :: get_value_real_array => dict_get_value_real_array
        PROCEDURE, PRIVATE :: get_value_float_array => dict_get_value_float_array
        PROCEDURE, PRIVATE :: get_value_logical => dict_get_value_logical
        PROCEDURE, PRIVATE :: get_value_logical_array => dict_get_value_logical_array
        GENERIC :: get => get_value_str_wrap &
                                , get_value_int &
                                , get_value_int_array &
                                , get_value_real &
                                , get_value_real_array &
                                , get_value_float &
                                , get_value_float_array &
                                , get_value_logical &
                                , get_value_logical_array

    end type DICT_STRUCT   

    ! INTERFACE DICT_STRUCT
    !     PROCEDURE dict_create
    ! END INTERFACE 

    ! We do not want everything to be public
    PRIVATE
    PUBLIC :: DICT_KEY_LENGTH, DICT_STRUCT, dict_list_of_keys &
            , PATH_SEP,get_parent_path, ASSIGNMENT(=),dict_create

CONTAINS

    function dict_create(parent) result(dict)
        IMPLICIT NONE
        class(DICT_STRUCT), pointer :: dict
        class(DICT_STRUCT), intent(in), optional, target :: parent
		
		allocate(dict)
        call dict%init(parent)
    end function dict_create

    subroutine dict_init(self,parent)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        class(DICT_STRUCT), intent(in), optional, target :: parent
        integer :: i

        allocate( self%table(HASH_SIZE) )

        do i = 1,HASH_SIZE
            self%table(i)%list => null()
        enddo
        self%num_of_entries=0
		if(present(parent)) then 
			self%parent_dict => parent
		else	
			self%parent_dict => null()
		endif

    end subroutine dict_init


    function dict_num_of_entries( self )
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        integer :: dict_num_of_entries

        dict_num_of_entries = self%num_of_entries

    end function dict_num_of_entries

    subroutine dict_list_of_keys(dict,keys,is_sub)
        IMPLICIT NONE
        class(DICT_STRUCT), INTENT(in)  :: dict
        CHARACTER(*), allocatable, INTENT(INOUT) :: keys(:)
        LOGICAL, allocatable, optional, INTENT(inout) :: is_sub(:)
        INTEGER :: i,count
        CLASS(LINKED_LIST), pointer :: elem
        CLASS(LIST_DATA), pointer :: data

        if(allocated(keys)) deallocate(keys)
        allocate(keys(dict%num_of_entries))
        if(present(is_sub)) then
            if(allocated(is_sub)) deallocate(keys)
            allocate(is_sub(dict%num_of_entries))
        endif

        count=0
        do i=1,hash_size
            elem => dict%table(i)%list
            do while(associated(elem))
                data => elem%get_data()
                SELECT TYPE(data)
                CLASS is (HASH_DATA)
                    count=count+1
                    keys(count)=data%key
                    if(present(is_sub)) &
                        is_sub(count)=.NOT. data%is_leaf
                    if(count >= dict%num_of_entries) return
                    elem => elem%get_next()
                END SELECT
            enddo
        enddo

    end subroutine dict_list_of_keys

    recursive subroutine dict_print_rec(self,unit,tab_in)
        use, intrinsic :: iso_fortran_env, only : output_unit
        IMPLICIT NONE
        CLASS(DICT_STRUCT), INTENT(in)  :: self
        CHARACTER(*), INTENT(in), optional :: tab_in
        CLASS(DICT_STRUCT), pointer  :: sub_dict
        INTEGER, INTENT(in), OPTIONAL :: unit
        CHARACTER(DICT_KEY_LENGTH), allocatable :: keys(:)
        LOGICAL, allocatable :: is_sub(:)
        CHARACTER(:), ALLOCATABLE :: tab,prompt
       ! TYPE(STRING_WRAPPER) :: wrap
        INTEGER :: i,u

        if(present(tab_in)) then
            tab=tab_in
        else
            tab=""
        endif
        if(present(unit)) then
            u=unit
        else
            u=output_unit
        endif

        call dict_list_of_keys(self,keys,is_sub)
        DO i=1,size(keys)
            if(is_sub(i)) then
                write(u,*) tab//trim(keys(i))//"{"
                sub_dict => self%get_sub_dict(keys(i))
                call dict_print_rec(sub_dict,u,tab//"  ")
                write(u,*)  tab//"}"
            else
              !  wrap=self%get_value(keys(i))
                write(u,*) tab//trim(keys(i))//" = "//self%get_value(keys(i))
            endif
        ENDDO
    end subroutine dict_print_rec

    ! dict_destroy --
    !     Destroy an entire dictionary
    ! Arguments:
    !     dict       Pointer to the dictionary to be destroyed
    ! Note:
    !     This version assumes that there are no
    !     pointers within the data that need deallocation
    !
    subroutine dict_destroy( self )
        class(DICT_STRUCT) :: self
        integer :: i

        do i = 1,size(self%table)
            if ( associated( self%table(i)%list ) ) then
                call self%table(i)%list%destroy()
            endif
        enddo
        deallocate( self%table )
        self%num_of_entries = -1

    end subroutine dict_destroy

    subroutine hash_data_destroy(self)
        IMPLICIT NONE
        CLASS(HASH_DATA) :: self

        if(allocated(self%value)) &
            deallocate(self%value)
        if(associated(self%sub_dict)) then
            call self%sub_dict%destroy()
            deallocate(self%sub_dict)
        endif
            
    end subroutine hash_data_destroy

    ! dict_add_key
    !     Add a new key
    ! Arguments:
    !     dict       Pointer to the dictionary
    !     key        Key for the new element
    !     value      Value for the new element
    ! Note:
    !     If the key already exists, the
    !     key's value is simply replaced
    !
    ! subroutine dict_add_value( self, key, value )
    !     class(DICT_STRUCT) :: self
    !     character(*), intent(in) :: key
    !     CHARACTER(*), intent(in)  :: value

    !     type(HASH_DATA)  :: data
    !     class(LINKED_LIST), pointer :: elem
    !     integer :: hash

    !     if(self%num_of_entries <= 0) then
    !         call self%init()
    !     endif

    !     data%key = key
    !     IF(CASE_INSENSITIVE) data%key=to_lower_case(data%key)
    !     data%is_leaf = .TRUE.
    !     data%value=value

    !     elem => self%get_elem(key)
    !     if ( associated(elem) ) then  
    !         !WRITE OVER EXISTING ELEMENT
    !         call elem%put_data(data)
    !     else
    !         !ADD NEW ELEMENT         
    !         hash = dict_hashkey( trim(key) )
    !         if ( associated( self%table(hash)%list ) ) then
    !             call self%table(hash)%list%append(data)
    !         else
    !             self%table(hash)%list => LINKED_LIST(data)
    !         endif
    !         self%num_of_entries=self%num_of_entries+1
    !     endif

    ! end subroutine dict_add_value

    subroutine dict_add_value_rec( self, path, value )
        class(DICT_STRUCT) :: self
        CHARACTER(*), intent(in) :: path
        CHARACTER(*), intent(in)  :: value
        type(HASH_DATA)  :: data

        data%is_leaf = .TRUE.
        data%value=value

        call dict_add_entry_rec(self,path,data)

    end subroutine dict_add_value_rec

    subroutine dict_add_sub_dict( self, path, sub_dict )
        class(DICT_STRUCT), target :: self
        character(len=*), intent(in) :: path
        class(DICT_STRUCT), intent(in), optional, target  :: sub_dict
        type(HASH_DATA)  :: data
        CHARACTER(DICT_KEY_LENGTH), allocatable :: key_split(:)
        INTEGER :: i,depth
		CHARACTER(:), ALLOCATABLE :: parent_path
        
        call dict_key_split(path,key_split)
        depth=size(key_split)

        if(present(sub_dict)) then
            data%sub_dict => sub_dict
        else
            data%sub_dict => dict_create()
        endif
        data%is_leaf = .FALSE.

		if(depth==1) then
			data%sub_dict%parent_dict => self
		else
			parent_path=""
			do i=1,depth-1
				parent_path=parent_path//trim(key_split(i))//PATH_SEP
			enddo
			data%sub_dict%parent_dict => self%get_sub_dict(parent_path)
		endif

        call dict_add_entry_rec(self,path,data)

    end subroutine dict_add_sub_dict


    subroutine dict_add_entry_rec( self, path, dict_entry )
        class(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        class(HASH_DATA), intent(inout)  :: dict_entry
        CHARACTER(DICT_KEY_LENGTH), allocatable :: key_split(:)
        class(DICT_STRUCT), pointer :: curr_dict
        class(LINKED_LIST), pointer :: elem
        class(LIST_DATA), pointer :: data
        INTEGER :: i,depth
        integer :: hash
        CHARACTER(:), ALLOCATABLE :: path_out
		
		path_out=trim(path)
        IF(CASE_INSENSITIVE) &
			path_out=to_lower_case(path_out)

        call dict_key_split(path_out,key_split)
        depth=size(key_split)
        curr_dict => self
        do i=1,depth
            elem => curr_dict%get_elem(key_split(i))
            if(associated(elem)) then

                data => elem%get_data()
                SELECT TYPE(data)
                CLASS IS (HASH_DATA)

                    if(i==depth) then
                        if(.not. data%is_leaf) &
                            write(0,*) "Warning: overriding a sub-dictionary at '"//trim(path)//"' !"
                        dict_entry%key=key_split(i)
                        call elem%put_data(dict_entry)
                        
                    elseif(data%is_leaf) then
                        write(0,*) "Error: wrong path '"//trim(path)//"' ! Hit a leaf at depth",i
                        STOP "Execution stopped."
                    else
                        curr_dict => data%sub_dict
                    endif 

                CLASS DEFAULT
                    STOP "Error: type error in dictionary!"
                END SELECT                

            elseif(i==depth) then

                !ADD NEW ELEMENT   
                dict_entry%key=key_split(i) 
                hash = dict_hashkey( trim(key_split(i)) )
                if ( associated( curr_dict%table(hash)%list ) ) then
                    call curr_dict%table(hash)%list%append(dict_entry)
                else
                    curr_dict%table(hash)%list => LINKED_LIST(dict_entry)
                endif
                curr_dict%num_of_entries=curr_dict%num_of_entries+1

            else
                write(0,*) "Error: wrong path '"//trim(path)//"' ! Hit a non-existing key at depth",i
                STOP "Execution stopped."
            endif
        enddo

    end subroutine dict_add_entry_rec

    ! dict_delete_key
    !     Delete a key-value pair from the dictionary
    ! Arguments:
    !     dict       Dictionary in question
    !     key        Key to be removed
    !
    subroutine dict_delete_key( self, key )
        class(DICT_STRUCT) :: self
        character(len=*), intent(in) :: key

        class(LINKED_LIST), pointer :: elem
        integer :: hash
        CHARACTER(:), ALLOCATABLE :: key_out

        key_out=key
        IF(CASE_INSENSITIVE) key_out=to_lower_case(key_out)

        elem => self%get_elem(key_out)
        if ( associated(elem) ) then
            hash = dict_hashkey( trim(key_out) )
            call self%table(hash)%list%delete_element(elem)
            self%num_of_entries=self%num_of_entries-1
        endif
    end subroutine dict_delete_key

    function dict_get_value_rec( self, path, default ) result(value)
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        character(*), intent(in), optional :: default
       ! TYPE(STRING_WRAPPER) :: value
        CHARACTER(:), ALLOCATABLE :: value
        CLASS(HASH_DATA), pointer :: dict_entry

        dict_entry => self%get_entry_rec(path)
        if(associated(dict_entry)) then
            if(dict_entry%is_leaf) then
                value = dict_entry%value
            else
                write(0,*) "Error: key '"//trim(path)//"' is a sub-dictionary and not a leaf!"
                STOP "Execution stopped."
            endif
        else    
            if(present(default)) then
                value=default
            else
                write(0,*) "Error: path '"//trim(path)//"' does not exist in dictionary!"
                STOP "Execution stopped."
            endif
        endif
        
    end function dict_get_value_rec

    function dict_get_sub_dict( self, path ) result(sub_dict)
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        class(DICT_STRUCT), pointer :: sub_dict
        CLASS(HASH_DATA), pointer :: dict_entry

        dict_entry => self%get_entry_rec(path)
        if(associated(dict_entry)) then
            if(dict_entry%is_leaf) then
                write(0,*) "Error: key '"//trim(path)//"' is a leaf and not a sub-dictionary!"
                STOP "Execution stopped."
            else
                sub_dict => dict_entry%sub_dict
            endif
        else    
            write(*,*) "Error: path '"//trim(path)//"' does not exist in dictionary!"
        endif
        
    end function dict_get_sub_dict

    ! dict_get_key
    !     Get the value belonging to a key
    ! Arguments:
    !     dict       Pointer to the dictionary
    !     key        Key for which the values are sought
    !
    ! function dict_get_value( self, key, default ) result(value)
    !     class(DICT_STRUCT) :: self
    !     character(len=*), intent(in) :: key
    !     character(*), intent(in), optional :: default
    !    ! TYPE(STRING_WRAPPER) :: value
    !     CHARACTER(:), ALLOCATABLE :: value
    !     class(HASH_DATA), pointer :: dict_entry

    !     dict_entry => self%get_entry(key)
    !     if(associated(dict_entry)) then
    !         if(dict_entry%is_leaf) then
    !             value = dict_entry%value
    !         else
    !             write(0,*) "Error: key '"//trim(key)//"' is a sub-dictionary and not a leaf!"
    !             STOP "Execution stopped."
    !         endif
    !     else
    !         if(present(default)) then
    !             value=default
    !         else
    !             write(0,*) "Error: key '"//trim(key)//"' does not exist in dictionary!"
    !             STOP "Execution stopped."
    !         endif
    !     endif
    ! end function dict_get_value

    ! dict_has_key
    !     Check if the dictionary has a particular key
    ! Arguments:
    !     dict       Pointer to the dictionary
    !     key        Key to be sought
    !
    ! function dict_has_key( self, key ) result(has)
    !     IMPLICIT NONE
    !     class(DICT_STRUCT) :: self
    !     character(len=*), intent(in) :: key
    !     logical :: has
    !     class(LINKED_LIST), pointer :: elem

    !     elem => self%get_elem(key)
    !     has = associated(elem)
    ! end function dict_has_key

    function dict_has_key_rec(self,path) result(has)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        character(len=*), intent(in) :: path
        logical :: has
        class(HASH_DATA), pointer :: dict_entry
        
        dict_entry => self%get_entry_rec(path)
        has = associated(dict_entry)
    end function dict_has_key_rec

    ! dict_get_elem
    !     Find the element with a particular key
    ! Arguments:
    !     dict       Pointer to the dictionary
    !     key        Key to be sought
    !
    function dict_get_elem( self, key ) result(elem)
        class(DICT_STRUCT) :: self
        character(len=*), intent(in) :: key

        class(LINKED_LIST), pointer   :: elem
        class(LIST_DATA), pointer :: data
        integer :: hash
        CHARACTER(:), ALLOCATABLE :: key_out

        key_out=trim(key)
        IF(CASE_INSENSITIVE) key_out=to_lower_case(key_out)


        hash = dict_hashkey( key_out )
        
        elem => self%table(hash)%list
        do while ( associated(elem) )
            data => elem%get_data()
            SELECT TYPE(data)
            CLASS IS (HASH_DATA)
                if ( to_lower_case(trim(data%key)) == key_out ) then
                    return
                else
                    elem => elem%get_next()
                endif
            CLASS DEFAULT
                STOP "Error: type error in dictionary!"
            END SELECT
        enddo
    end function dict_get_elem

    function dict_get_entry( self, key ) result(dict_entry)
        class(DICT_STRUCT) :: self
        character(len=*), intent(in) :: key
        class(HASH_DATA), pointer :: dict_entry
        class(LINKED_LIST), pointer   :: elem
        class(LIST_DATA), pointer :: data
        integer :: hash
        CHARACTER(:), ALLOCATABLE :: key_out

        key_out=trim(key)
        IF(CASE_INSENSITIVE) key_out=to_lower_case(key_out)

        hash = dict_hashkey( key_out )

        dict_entry => null()
        
        elem => self%table(hash)%list
        do while ( associated(elem) )
            data => elem%get_data()
            SELECT TYPE(data)
            CLASS IS (HASH_DATA)
                if ( to_lower_case(trim(data%key ))== key_out ) then
                    dict_entry => data
                    return
                else
                    elem => elem%get_next()
                endif
            CLASS DEFAULT
                STOP "Error: type error in dictionary!"
            END SELECT
        enddo
    end function dict_get_entry

    function dict_get_entry_rec( self, path ) result(dict_entry)
        class(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        CHARACTER(DICT_KEY_LENGTH), allocatable :: key_split(:)
        class(DICT_STRUCT), pointer :: curr_dict
        class(HASH_DATA), pointer :: dict_entry
        INTEGER :: i,depth
        CHARACTER(:), ALLOCATABLE :: path_out

        path_out=trim(path)
        IF(CASE_INSENSITIVE) path_out=to_lower_case(path_out)

        call dict_key_split(path_out,key_split)
        depth=size(key_split)

        dict_entry=>null()
        curr_dict => self
        do i=1,depth
            dict_entry => curr_dict%get_entry(key_split(i))
            if(associated(dict_entry)) then
                if(i==depth) then
                    RETURN
                elseif(dict_entry%is_leaf) then
                    write(0,*) "Warning: wrong path '"//trim(path)//"' ! Hit a leaf at depth",i
                    dict_entry => null()
                    RETURN
                    !STOP "Execution stopped."
                else
                    curr_dict => dict_entry%sub_dict
                endif                
            elseif(i/=depth) then
                !write(0,*) "Warning: wrong path '"//trim(path)//"' ! Hit a non-existing key at depth",i
                dict_entry => null()
                RETURN
                !STOP "Execution stopped."
            else
                RETURN
            endif
        enddo
        
    end function dict_get_entry_rec


    ! dict_hashkey
    !     Determine the hash value from the string
    ! Arguments:
    !     key        String to be examined
    !
    integer function dict_hashkey( key )
        character(len=*), intent(in) :: key
        integer                      :: i

        dict_hashkey = 0

        do i = 1,len(key)
            dict_hashkey = multiplier * dict_hashkey + ichar(key(i:i))
            dict_hashkey = 1 + mod( dict_hashkey-1, hash_size )
        enddo
        
       ! dict_hashkey = 1 + mod( dict_hashkey-1, hash_size )
    end function dict_hashkey

    subroutine dict_key_split(path,key_split)
        IMPLICIT NONE
        CHARACTER(*), INTENT(in) :: path
        CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE, INTENT(out) :: key_split(:)
        CHARACTER(:), allocatable :: path_clean
        INTEGER :: i, n, last_sep, n_sep, counter

        path_clean=clean_path(path)
        n=len(path_clean)

        n_sep=0
        do i=1,n
            if(path_clean(i:i)==PATH_SEP) n_sep=n_sep+1
        enddo
        if(allocated(key_split)) deallocate(key_split)
        allocate(key_split(n_sep+1))
        if(n_sep==0) then
            key_split(1)=path_clean
            return
        endif
        counter=1
        last_sep=0
        do i=1,n
            if(path_clean(i:i)==PATH_SEP .or. i==n) then
                !if(last_sep+1 < i-1) then      
                    if(i==n) then    
                        key_split(counter)=path_clean(last_sep+1:n)
                    else
                        key_split(counter)=path_clean(last_sep+1:i-1)
                    endif
                    counter=counter+1
               ! endif
                last_sep=i
            endif
        enddo

    end subroutine dict_key_split

    function get_parent_path(path) result(parent)
        IMPLICIT NONE
        CHARACTER(*), INTENT(in) :: path
        CHARACTER(:), ALLOCATABLE :: parent
        CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: key_split(:)
        INTEGER :: i
        
        parent=""
        call dict_key_split(path,key_split)
        if(size(key_split)<=1) return

        parent=key_split(1)
        DO i=2,size(key_split)-1
            parent=parent//PATH_SEP//key_split(i)
        ENDDO

    end function get_parent_path

    function clean_path(path) result(path_clean)
        IMPLICIT NONE
        CHARACTER(*), intent(in) :: path
        CHARACTER(:), ALLOCATABLE :: path_clean
        INTEGER :: i,n,last_sep
        CHARACTER(1) :: prev_char
        CHARACTER(:), ALLOCATABLE :: path_clean_2

        path_clean=trim(path)
        n=len(path_clean)
        !remove leading separators
        do while(path_clean(n:n)==PATH_SEP)
            path_clean=path_clean(1:n-1)
            n=n-1
        enddo
        !remove trailing separators
        do while(path_clean(1:1)==PATH_SEP)
            path_clean=path_clean(2:n)
            n=n-1
        enddo
        !remove separator repetitions
        prev_char=""
        path_clean_2=path_clean
        last_sep=-1
        do i=1,n
            IF(path_clean(i:i)==PATH_SEP) THEN
                IF(prev_char==PATH_SEP) THEN
                    path_clean_2=path_clean(1:last_sep)
                    IF(i+1<=n) &
                        path_clean_2=path_clean_2//path_clean(i+1:n)
                ELSE
                    last_sep=i
                ENDIF
            ENDIF
            prev_char=path_clean(i:i)           
        enddo
        path_clean=path_clean_2

        !check if something is left
        n=len(path_clean)
        if(n<=0) then
            write(0,*) "Error: '"//trim(path)//"' is not a correct path!"
            STOP "Execution stopped."
        endif

    end function clean_path

!-----------------------------------------------------
! TYPE SPECIFIC ADD

    subroutine dict_add_value_int(self, key, value)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        character(*), intent(in) :: key
        INTEGER, INTENT(in) :: value
        CHARACTER(256) :: value_char

        write(value_char,*) value
        call self%add_value_string(key,trim(ADJUSTL(value_char)))

    end subroutine dict_add_value_int

    subroutine dict_add_value_int_array(self, key, value)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        character(*), intent(in) :: key
        INTEGER, INTENT(in) :: value(:)
        CHARACTER(256) :: value_char_elem
        CHARACTER(:), ALLOCATABLE :: value_char
        INTEGER :: i

        value_char=""
        DO i=1,size(value)
            write(value_char_elem,*) value(i)
            value_char=value_char//" "//trim(ADJUSTL(value_char_elem)) 
        ENDDO

        call self%add_value_string(key,trim(value_char))

    end subroutine dict_add_value_int_array

    subroutine dict_add_value_float(self, key, value)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        character(*), intent(in) :: key
        REAL, INTENT(in) :: value
        CHARACTER(256) :: value_char

        write(value_char,*) value
        call self%add_value_string(key,trim(ADJUSTL(value_char)))

    end subroutine dict_add_value_float

    subroutine dict_add_value_float_array(self, key, value)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        character(*), intent(in) :: key
        REAL, INTENT(in) :: value(:)
        CHARACTER(256) :: value_char_elem
        CHARACTER(:), ALLOCATABLE :: value_char
        INTEGER :: i

        value_char=""
        DO i=1,size(value)
            write(value_char_elem,*) value(i)
            value_char=value_char//" "//trim(ADJUSTL(value_char_elem)) 
        ENDDO

        call self%add_value_string(key,trim(value_char))

    end subroutine dict_add_value_float_array

    subroutine dict_add_value_real(self, key, value)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        character(*), intent(in) :: key
        REAL(dp), INTENT(in) :: value
        CHARACTER(256) :: value_char

        write(value_char,*) value
        call self%add_value_string(key,trim(ADJUSTL(value_char)))

    end subroutine dict_add_value_real

    subroutine dict_add_value_real_array(self, key, value)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        character(*), intent(in) :: key
        REAL(dp), INTENT(in) :: value(:)
        CHARACTER(256) :: value_char_elem
        CHARACTER(:), ALLOCATABLE :: value_char
        INTEGER :: i

        value_char=""
        DO i=1,size(value)
            write(value_char_elem,*) value(i)
            value_char=value_char//" "//trim(ADJUSTL(value_char_elem)) 
        ENDDO

        call self%add_value_string(key,trim(value_char))

    end subroutine dict_add_value_real_array

    subroutine dict_add_value_logical(self, key, value)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        character(*), intent(in) :: key
        LOGICAL, INTENT(in) :: value
        CHARACTER(:), ALLOCATABLE :: value_char

        if(value) then
            value_char=".TRUE."
        else
            value_char=".FALSE."
        endif
        call self%add_value_string(key,value_char)

    end subroutine dict_add_value_logical

    subroutine dict_add_value_logical_array(self, key, value)
        IMPLICIT NONE
        class(DICT_STRUCT) :: self
        character(*), intent(in) :: key
        LOGICAL, INTENT(in) :: value(:)
        CHARACTER(:), ALLOCATABLE :: value_char
        INTEGER :: i

        value_char=""
        DO i=1,size(value) 
            if(value(i)) then
                value_char=value_char//" .TRUE."
            else
                value_char=value_char//" .FALSE."
            endif
        ENDDO
        call self%add_value_string(key,trim(value_char))

    end subroutine dict_add_value_logical_array

!---------------------------------------------

    function dict_get_value_int(self,path,default) result(value)
        IMPLICIT NONE
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        INTEGER, intent(in) :: default
        INTEGER :: value
        !TYPE(STRING_WRAPPER) :: output
        CHARACTER(:), ALLOCATABLE :: output

        output=self%get_value_str_wrap(path,default="")
        if(output=="") then
            value=default
        else
            value=output
        endif
    end function dict_get_value_int

    function dict_get_value_int_array(self,path,default) result(value)
        IMPLICIT NONE
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        INTEGER, intent(in) :: default(:)
        INTEGER, ALLOCATABLE :: value(:)
        !TYPE(STRING_WRAPPER) :: output
        CHARACTER(:), ALLOCATABLE :: output

        output=self%get_value_str_wrap(path,default="")
        if(output=="") then
            allocate(value(size(default)))
            value=default
        else
            value=output
        endif
    end function dict_get_value_int_array

    function dict_get_value_real(self,path,default) result(value)
        IMPLICIT NONE
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        REAL(dp), intent(in) :: default
        REAL(dp) :: value
        !TYPE(STRING_WRAPPER) :: output
        CHARACTER(:), ALLOCATABLE :: output

        output=self%get_value_str_wrap(path,default="")
        if(output=="") then
            value=default
        else
            value=output
        endif
    end function dict_get_value_real

    function dict_get_value_real_array(self,path,default) result(value)
        IMPLICIT NONE
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        REAL(dp), intent(in) :: default(:)
        REAL(dp), ALLOCATABLE :: value(:)
        !TYPE(STRING_WRAPPER) :: output
        CHARACTER(:), ALLOCATABLE :: output

        output=self%get_value_str_wrap(path,default="")
        if(output=="") then
            allocate(value(size(default)))
            value=default
        else
            value=output
        endif
    end function dict_get_value_real_array

    function dict_get_value_float(self,path,default) result(value)
        IMPLICIT NONE
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        REAL, intent(in) :: default
        REAL(dp) :: value
        !TYPE(STRING_WRAPPER) :: output
        CHARACTER(:), ALLOCATABLE :: output

        output=self%get_value_str_wrap(path,default="")
        if(output=="") then
            value=REAL(default,dp)
        else
            value=output
        endif
    end function dict_get_value_float

    function dict_get_value_float_array(self,path,default) result(value)
        IMPLICIT NONE
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        REAL, intent(in) :: default(:)
        REAL(dp), ALLOCATABLE :: value(:)
        !TYPE(STRING_WRAPPER) :: output
        CHARACTER(:), ALLOCATABLE :: output

        output=self%get_value_str_wrap(path,default="")
        if(output=="") then
            allocate(value(size(default)))
            value=REAL(default,dp)
        else
            value=output
        endif
    end function dict_get_value_float_array

    function dict_get_value_logical(self,path,default) result(value)
        IMPLICIT NONE
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        LOGICAL, intent(in) :: default
        LOGICAL :: value
        !TYPE(STRING_WRAPPER) :: output
        CHARACTER(:), ALLOCATABLE :: output

        output=self%get_value_str_wrap(path,default="")
        if(output=="") then
            value=default
        else
            value=output
        endif
    end function dict_get_value_logical

    function dict_get_value_logical_array(self,path,default) result(value)
        IMPLICIT NONE
        CLASS(DICT_STRUCT),target :: self
        character(*), intent(in) :: path
        LOGICAL, intent(in) :: default(:)
        LOGICAL, ALLOCATABLE :: value(:)
       !TYPE(STRING_WRAPPER) :: output
        CHARACTER(:), ALLOCATABLE :: output

        output=self%get_value_str_wrap(path,default="")
        if(output=="") then
            allocate(value(size(default)))
            value=default
        else
            value=output
        endif
    end function dict_get_value_logical_array

END MODULE nested_dictionaries