module muller_potential
    use kinds
    IMPLICIT NONE

    real(dp), dimension(4) ::  A_p = (/-200._dp, -100._dp, -170._dp, 15._dp/)
    real(dp), dimension(4) ::  aa_p = (/-1._dp, -1._dp, -6.5_dp, 0.7_dp/)
    real(dp), dimension(4) ::  bb_p = (/0._dp, 0._dp, 11._dp, 0.6_dp/)
    real(dp), dimension(4) ::  cc_p = (/-10._dp, -10._dp, -6.5_dp, 0.7_dp/)
    real(dp), dimension(4) ::  xx_p = (/1._dp, 0._dp, -0.5_dp, -1._dp/)
    real(dp), dimension(4) ::  yy_p = (/0._dp, 0.5_dp, 1.5_dp, 1._dp/)

    PRIVATE
    PUBLIC :: Pot_muller,dPot_muller, hessian_muller

CONTAINS

    function Pot_muller(X) result(U)
		implicit none
		real(dp),intent(in)  :: X(:,:)
        REAL(dp) :: U
        INTEGER :: it
		
		U  = 0._dp
        do it = 1, 4
            U = U + A_p(it)*exp( aa_p(it)*(X(1,1)-xx_p(it))**2 + bb_p(it)*(X(1,1) - xx_p(it))*(X(2,1)-yy_p(it)) + &
                cc_p(it)*(X(2,1)-yy_p(it))**2 )
        enddo
		
	end function Pot_muller

	function dPot_muller(X) result(dU)
		implicit none
		real(dp),intent(in)  :: X(:,:)
		real(dp) :: dU(size(X,1),size(X,2))
        INTEGER :: it
        REAL(dp) :: alpha_x,alpha_y

		dU = 0._dp
        do it = 1, 4
            alpha_x = 2*aa_p(it)*(X(1,1)-xx_p(it)) + bb_p(it)*(X(2,1)-yy_p(it))
            dU(1,1) = dU(1,1) + alpha_x*A_p(it)*exp( aa_p(it)*(X(1,1)-xx_p(it))**2 + &
                bb_p(it)*(X(1,1) - xx_p(it))*(X(2,1)-yy_p(it)) +cc_p(it)*(X(2,1)-yy_p(it))**2 )

            alpha_y = bb_p(it)*(X(1,1) - xx_p(it)) + 2*cc_p(it)*(X(2,1)-yy_p(it))
            dU(2,1) = dU(2,1) + alpha_y*A_p(it)*exp( aa_p(it)*(X(1,1)-xx_p(it))**2 + bb_p(it)*(X(1,1) - &
                xx_p(it))*(X(2,1)-yy_p(it)) + cc_p(it)*(X(2,1)-yy_p(it))**2 )
        enddo
		
	end function dPot_muller

    function hessian_muller(X) result(H)
		implicit none
		real(dp),intent(in)  :: X(:,:)
		real(dp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))

		STOP "Hessian not implemented for Muller potential"
		
	end function hessian_muller


end module muller_potential