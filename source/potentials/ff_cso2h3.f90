module ff_cso2h3
  use kinds
  USE potential
  USE atomic_units
  USE nested_dictionaries
  USE string_operations, only: int_to_str
  IMPLICIT NONE

  ABSTRACT INTERFACE
		subroutine pot_sub(dr,v,dv)
         import dp
			IMPLICIT NONE
			REAL(dp), INTENT(in):: dr
         REAL(dp), INTENT(out) :: v,dv
		end subroutine pot_sub
   END INTERFACE

  TYPE potptr_type
      PROCEDURE(pot_sub), nopass, pointer :: p
  END TYPE potptr_type

  TYPE(potptr_type),SAVE ::  potptr(6,6)

  CHARACTER(*), PARAMETER :: LIB="POTENTIAL/ff_cso2h3/"

  REAL(dp), SAVE :: vo_oh, sig_oh, ro_oh, g_oh, ry_oh
  REAL(dp), SAVE :: vo_oo, sig_oo, ro_oo, g_oo, ry_oo
  REAL(dp), SAVE :: vo_cso, sig_cso, ro_cso, g_cso, ry_cso
  REAL(dp), SAVE :: vo_cscs, sig_cscs, ro_cscs, g_cscs,ry_cscs
  REAL(dp), SAVE :: g_csh, ry_csh
  REAL(dp), SAVE :: g_hh, ry_hh
  REAL(dp), SAVE :: k_bending,theta, cutwidth_bending, rcut_bending
  LOGICAL, SAVE :: apply_bending

  INTEGER, PARAMETER :: OMAXNB=3
  INTEGER, SAVE :: n_atoms,n_Cs,n_O,n_H
  INTEGER, ALLOCATABLE, SAVE :: Onblist(:,:), O_n_nb(:)
  TYPE(DICT_STRUCT), POINTER, SAVE :: elem_dict
  INTEGER, ALLOCATABLE, DIMENSION(:), SAVE :: indices_Cs, indices_H, indices_O
  REAL(dp) :: rmax_Onblist, rmax2_Onblist

  PRIVATE
  PUBLIC :: get_cso2h3_pot_info, Pot_cso2h3, dPot_cso2h3,hessian_cso2h3 &
            ,initialize_cso2h3

CONTAINS

SUBROUTINE initialize_cso2h3(param_library)
   IMPLICIT NONE
   TYPE(DICT_STRUCT), INTENT(IN) :: param_library
   TYPE(DICT_STRUCT), POINTER :: pot_lib
   INTEGER :: i
   LOGICAL :: isDeuterium

   pot_lib => param_library%get_child(LIB)
   elem_dict => param_library%get_child("ELEMENTS")

   n_atoms=param_library%get("PARAMETERS/n_atoms")   
   indices_Cs = elem_dict%get("cs/indices")
   indices_O = elem_dict%get("o/indices")
   isDeuterium=elem_dict%has_key("d")
   if(isDeuterium) then
      indices_H = elem_dict%get("d/indices")
   else
      indices_H = elem_dict%get("h/indices")
   endif
   n_Cs=SIZE(indices_Cs)
   n_O=SIZE(indices_O)
   n_H=SIZE(indices_H)
   allocate(Onblist(OMAXNB,n_O), O_n_nb(n_O))

   rmax_Onblist=pot_lib%get("onblist_rmax",default=1.3_dp/au%bohr)
   rmax2_Onblist=rmax_Onblist*rmax_Onblist
   
   vo_oh=pot_lib%get("vo_oh",default=0.35_dp/au%eV)
   sig_oh=pot_lib%get("sig_oh",default=6.13_dp*au%bohr)
   ro_oh=pot_lib%get("ro_oh",default=1.0_dp/au%bohr)

   vo_oo=pot_lib%get("vo_oo",default=0.15_dp/au%eV)
   sig_oo=pot_lib%get("sig_oo",default=4.0_dp*au%bohr)
   ro_oo=pot_lib%get("ro_oo",default=2.57_dp/au%bohr)

   vo_cso=pot_lib%get("vo_cso",default=0.1_dp/au%eV)
   sig_cso=pot_lib%get("sig_cso",default=1.0_dp*au%bohr)
   ro_cso=pot_lib%get("ro_cso",default=3.4604_dp/au%bohr)

   vo_cscs=pot_lib%get("vo_cscs",default=1.0_dp/au%eV)
   sig_cscs=pot_lib%get("sig_cscs",default=10.0_dp*au%bohr)
   ro_cscs=pot_lib%get("ro_cscs",default=4.47_dp/au%bohr)

   g_csh=pot_lib%get("g_csh",default=8.5_dp/(au%ev*au%bohr))
   ry_csh=pot_lib%get("ry_csh",default=2.272_dp/au%bohr)

   g_hh=pot_lib%get("g_hh",default=1.0_dp/(au%ev*au%bohr))
   ry_hh=pot_lib%get("ry_hh",default=1.5_dp/au%bohr)

   g_oh=pot_lib%get("g_oh",default=-1.0_dp/(au%ev*au%bohr))
   ry_oh=pot_lib%get("ry_oh",default=1.5_dp/au%bohr)

   g_cscs=pot_lib%get("g_cscs",default=1.0_dp/(au%ev*au%bohr))
   ry_cscs=pot_lib%get("ry_cscs",default=1.5_dp/au%bohr)

   g_oo=pot_lib%get("g_oo",default=1.0_dp/(au%ev*au%bohr))
   ry_oo=pot_lib%get("ry_oo",default=2.6_dp/au%bohr)

   g_cso=pot_lib%get("g_cso",default=1.0_dp/(au%ev*au%bohr))
   ry_cso=pot_lib%get("ry_cso",default=2.6_dp/au%bohr)

   apply_bending=pot_lib%get("apply_bending",default=.TRUE.)
   k_bending=pot_lib%get("k_bending",default=87.85_dp/au%kcalpermol)
   theta=pot_lib%get("theta_bending",default=107.4_dp)*pi/180._dp
   rcut_bending=pot_lib%get("rcut_bending",default=1.9_dp/au%bohr)
   cutwidth_bending=pot_lib%get("cutwidth_bending",default=0.02/au%bohr)
   

   potptr(1,1)%p=>Morse_cscs
   potptr(1,2)%p=>Morse_cso   
   potptr(1,3)%p=>Morse_cso
   potptr(1,4)%p=>yukawa_csh
   potptr(1,5)%p=>yukawa_csh
   potptr(1,6)%p=>yukawa_csh

   DO i=2,3
      potptr(i,1)%p=>Morse_cso
      potptr(i,2)%p=>Morse_oo
      potptr(i,3)%p=>Morse_oo
      potptr(i,4)%p=>Morse_oh
      potptr(i,5)%p=>Morse_oh
      potptr(i,6)%p=>Morse_oh
   ENDDO

   DO i=4,6
      potptr(i,1)%p=>yukawa_csh
      potptr(i,2)%p=>Morse_oh
      potptr(i,3)%p=>Morse_oh
      potptr(i,4)%p=>yukawa_hh
      potptr(i,5)%p=>yukawa_hh
      potptr(i,6)%p=>yukawa_hh
   ENDDO


END SUBROUTINE initialize_cso2h3

SUBROUTINE get_cso2h3_pot_info(X,Pot,Forces,hessian,vir)
   IMPLICIT NONE
   real(dp), intent(in)  :: X(:,:)
   real(dp), intent(out) :: Forces(size(X,1),size(X,2))
   real(dp), intent(out) :: Pot
   real(dp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
   real(dp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))
   real(dp) :: vir_tmp(size(X,2),size(X,2))
   real(dp) :: box(3)
   INTEGER :: na

   call ffcso2h3(X,size(X,1),Forces,Pot,vir_tmp)
   if(present(vir)) vir = vir_tmp
   if(present(hessian)) hessian = hessian_cso2h3(X)

END SUBROUTINE get_cso2h3_pot_info

function Pot_cso2h3(X) result(U)
   implicit none
   real(dp),intent(in)  :: X(:,:)
   REAL(dp) :: U
   INTEGER :: it
   REAL(dp) :: F(size(X,1),size(X,2))
   real(dp) :: vir_tmp(3,3)

   call ffcso2h3(X,size(X,1),F,U,vir_tmp)

end function Pot_cso2h3

function dPot_cso2h3(X) result(dU)
   implicit none
   real(dp),intent(in)  :: X(:,:)
   real(dp) :: dU(size(X,1),size(X,2))
   INTEGER :: it
   REAL(dp) :: alpha_x,alpha_y
   real(dp) :: vir_tmp(3,3), U

   call ffcso2h3(X,size(X,1),dU,U,vir_tmp)

   dU=-dU

end function dPot_cso2h3

function hessian_cso2h3(X) result(H)
   implicit none
   real(dp),intent(in)  :: X(:,:)
   real(dp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))

   STOP "Hessian not implemented for cso2h3 potential"
   
end function hessian_cso2h3

subroutine ffcso2h3(rt,na,dvdrt,v, vir)
  use atomic_units
  implicit none  
  integer :: i,j,na,nm,ic,njump
  real(dp) :: r(3,na), dvdr(3,na), rt(na,3), dvdrt(na,3)
  real(dp) :: oo_eps, oo_sig, rcut,v,vlj,vint
  real(dp) :: ks,kb, angstrom,alp,de
  real(dp) :: qc,qo,theta,reco,vir(3,3), virlj(3,3), virint(3,3)

  ! Set and check number of molecules.
  ! 
  if (mod(na,6)/=0) stop 'ERROR 1 in POTENTIAL'

  ! Zero-out potential and derivatives.
  !
  v = 0.d0
  vir = 0.d0
  dvdr(:,:) = 0.d0

  do i=1, na 
    r(:,i)=rt(i,:)
  enddo


  call interact_cso2h3(r,dvdr,v,vir,na,njump,oo_eps,oo_sig,rcut)

   if(apply_bending) then
      call build_Onblist(rt)
      call intra_harm_harm(r,dvdr,na,vint,virint)
      v = v + vint
      vir = vir + virint
   endif

  ! write(*,*) "force="
  ! write(*,*) dvdr

  ! ....and we're done...
  !
  vir = -1.0d0 *vir
  do i=1, na 
    dvdrt(i,:) = -dvdr(:,i)
  enddo
  return
end Subroutine ffcso2h3

Subroutine interact_cso2h3(r,dvdr,v,vir,na,njump,oo_eps,oo_sig,rcut)
  implicit none
  integer :: na,i,j,njump
  real(dp) :: r(3,na),dvdr(3,na),v,vir(3,3)
  real(dp) :: oo_eps,oo_sig,rcut,ptail
  real(dp) :: sigsq,rcutsq,vij
  real(dp) :: dr,onr2,fij,dfx,dfy,dfz,sr2,sr6,wij
  real(dp) :: dx,dy,dz,vscale,dscale
  integer :: icell_i,iat_i,icell_j,iat_j
  real(dp) :: dq(3)
  rcutsq = rcut*rcut
  
  v = 0.d0
  dvdr(:,:) = 0.d0
  vir = 0.d0

  do i = 1,na-1
    iat_i=mod(i-1,6)+1
     do j = i+1,na
        iat_j=mod(j-1,6)+1        
        dx = r(1,i)-r(1,j)
        dy = r(2,i)-r(2,j)
        dz = r(3,i)-r(3,j)
        dq=r_pbc((/dx,dy,dz/))
        dx=dq(1) ; dy=dq(2) ; dz=dq(3)        
      !   dx = dx - box(1)*nint(dx/box(1))
      !   dy = dy - box(2)*nint(dy/box(2))
      !   dz = dz - box(3)*nint(dz/box(3))
        dr = sqrt(dx*dx + dy*dy + dz*dz)
        !write(*,*) iat_i,iat_j,dr
        call potptr(iat_i,iat_j)%p(dr,v,fij)

         dfx = -fij * dx/dr
         dfy = -fij * dy/dr
         dfz = -fij * dz/dr
         dvdr(1,i) = dvdr(1,i) - dfx
         dvdr(2,i) = dvdr(2,i) - dfy
         dvdr(3,i) = dvdr(3,i) - dfz
         dvdr(1,j) = dvdr(1,j) + dfx
         dvdr(2,j) = dvdr(2,j) + dfy
         dvdr(3,j) = dvdr(3,j) + dfz
         vir(1,1) = vir(1,1) - dx * dfx
         vir(2,2) = vir(2,2) - dy * dfy
         vir(3,3) = vir(3,3) - dz * dfz
         vir(1,2) = vir(1,2) - dx * dfy
         vir(1,3) = vir(1,3) - dx * dfz
         vir(2,1) = vir(2,1) - dy * dfx
         vir(2,3) = vir(2,3) - dy * dfz
         vir(3,1) = vir(3,1) - dz * dfx
         vir(3,2) = vir(3,2) - dz * dfy
     enddo
  enddo 

end subroutine interact_cso2h3

subroutine intra_harm_harm(r,dvdr,na,v,vir)
   implicit none
   integer :: na,j,i1,i2,iO
   real(dp) :: r(3,na), dvdr(3,na), vir(3,3)
   real(dp) :: dr1,dr2,dr3,dq(3)
   real(dp) :: dx1,dy1,dz1,v,dx2,dy2,dz2,dr1sq,dr2sq,dr3dx3
   real(dp) :: dx3,dy3,dz3,dr3sq,dr1dx1,dr1dy1,dr1dz1
   real(dp) :: dr2dx2,dr2dy2,dr2dz2,dr3dy3,dr3dz3,u,vv,v2,arg,ang
   real(dp) :: dang,uprime,vprime,grad
   real(dp) :: dvdx1,dvdx2,dvdx3,dvdy1,dvdy2,dvdy3
   real(dp) :: dvdz1,dvdz2,dvdz3,dvdr1,dvdr2,dvdr3
   real(dp) :: dthetadr1,dthetadr2,dthetadr3
   real(dp) :: darg,dr1a,dr2a,dr3a
   real(dp) :: de,drasq,drbsq,cp
   real(dp) :: f1,f2,deb,a1,a2,a3,xx,yy,zz,xy,xz,yz
   real(dp) :: smooth1, smooth2, smooth
   real(dp) :: expsm,vbend, dsmooth1dr1,dsmooth2dr2
   real(dp) :: dsmoothdr1, dsmoothdr2

   v = 0.d0
   vir = 0.0d0

   ! LOOP OVER ALL O atoms
   DO j=1,n_O
     ! write(0,*) Onblist(:,j)
      if(O_n_nb(j)/=2) CYCLE

      iO=indices_O(j)      
      i1=Onblist(1,j)
      i2=Onblist(2,j)


      dq=r_pbc(r(:,i1)-r(:,iO))
      !dq=r(:,i1)-r(:,iO)
      dx1=dq(1) ; dy1=dq(2) ; dz1=dq(3)
      dr1sq = dx1*dx1 + dy1*dy1 + dz1*dz1
      dr1 = sqrt(dr1sq)
      dr1a = dr1
      dr1dx1 = dx1/dr1
      dr1dy1 = dy1/dr1
      dr1dz1 = dz1/dr1

      expsm=exp((dr1-rcut_bending)/cutwidth_bending)
      smooth1=1._dp/(1._dp+expsm)
      !if(smooth1<=TINY(1._dp)) CYCLE

      dsmooth1dr1=-expsm*smooth1/cutwidth_bending


      dq=r_pbc(r(:,i2)-r(:,iO))
      !dq=r(:,i2)-r(:,iO)
      dx2=dq(1) ; dy2=dq(2) ; dz2=dq(3)
      dr2sq = dx2*dx2 + dy2*dy2 + dz2*dz2
      dr2 = sqrt(dr2sq)
      dr2a = dr2
      dr2dx2 = dx2/dr2
      dr2dy2 = dy2/dr2
      dr2dz2 = dz2/dr2
      expsm=exp((dr2-rcut_bending)/cutwidth_bending)
      smooth2=1._dp/(1._dp+expsm)
      !if(smooth2<=TINY(1._dp)) CYCLE

      dsmooth2dr2=-expsm*smooth2/cutwidth_bending


      dq=r_pbc(r(:,i2)-r(:,i1))
      !dq=r(:,i2)-r(:,i1)
      dx3=dq(1) ; dy3=dq(2) ; dz3=dq(3)
      dr3sq = dx3*dx3 + dy3*dy3 + dz3*dz3
      dr3 = sqrt(dr3sq)
      dr3a = dr3
      dr3dx3 = dx3/dr3
      dr3dy3 = dy3/dr3
      dr3dz3 = dz3/dr3

      u = (dr1sq + dr2sq - dr3sq)
      vv = (2.d0 * dr1a * dr2a)         
      v2 = 1.d0/(vv * vv)
      arg = u / vv
      darg = -1.d0/sqrt(1.d0 - arg*arg)
      ang = acos( arg )
      dang = (ang - theta)
      uprime = 2.d0 * dr1a
      vprime = 2.d0 * dr2a
      grad = (uprime*vv - vprime*u) * v2
      dthetadr1= darg * grad
      uprime = 2.d0 * dr2a
      vprime = 2.d0 * dr1a
      grad = (uprime*vv - vprime*u) * v2
      dthetadr2= darg * grad
      uprime = -2.d0*dr3a
      grad = (uprime*vv) * v2
      dthetadr3= darg * grad

      !write(0,*) ang*180./pi

      smooth=smooth1*smooth2
      vbend=(0.5_dp*k_bending*dang**2)*smooth
      v = v + vbend
      a3 = k_bending*dang*smooth
      dsmoothdr1=vbend*dsmooth1dr1
      dsmoothdr2=vbend*dsmooth2dr2

      dvdr1 = a3*dthetadr1+dsmoothdr1
      dvdr2 = a3*dthetadr2+dsmoothdr2
      dvdr3 = a3*dthetadr3
      dvdx1 = dvdr1*dr1dx1
      dvdy1 = dvdr1*dr1dy1
      dvdz1 = dvdr1*dr1dz1 
      dvdx2 = dvdr2*dr2dx2
      dvdy2 = dvdr2*dr2dy2
      dvdz2 = dvdr2*dr2dz2
      dvdx3 = dvdr3*dr3dx3
      dvdy3 = dvdr3*dr3dy3
      dvdz3 = dvdr3*dr3dz3
      dvdr(1,iO) = dvdr(1,iO) - dvdx1 - dvdx2
      dvdr(2,iO) = dvdr(2,iO) - dvdy1 - dvdy2
      dvdr(3,iO) = dvdr(3,iO) - dvdz1 - dvdz2
      dvdr(1,i1) = dvdr(1,i1) + dvdx1 - dvdx3
      dvdr(2,i1) = dvdr(2,i1) + dvdy1 - dvdy3
      dvdr(3,i1) = dvdr(3,i1) + dvdz1 - dvdz3
      dvdr(1,i2) = dvdr(1,i2) + dvdx2 + dvdx3
      dvdr(2,i2) = dvdr(2,i2) + dvdy2 + dvdy3
      dvdr(3,i2) = dvdr(3,i2) + dvdz2 + dvdz3

      xx = dx1*dvdx1 + dx2*dvdx2 + dx3*dvdx3
      xy = dx1*dvdy1 + dx2*dvdy2 + dx3*dvdy3
      xz = dx1*dvdz1 + dx2*dvdz2 + dx3*dvdz3
      yy = dy1*dvdy1 + dy2*dvdy2 + dy3*dvdy3
      yz = dy1*dvdz1 + dy2*dvdz2 + dy3*dvdz3
      zz = dz1*dvdz1 + dz2*dvdz2 + dz3*dvdz3
      vir(1,1) = vir(1,1) + xx
      vir(1,2) = vir(1,2) + xy
      vir(1,3) = vir(1,3) + xz
      vir(2,1) = vir(2,1) + xy
      vir(2,2) = vir(2,2) + yy
      vir(2,3) = vir(2,3) + yz
      vir(3,1) = vir(3,1) + xz
      vir(3,2) = vir(3,2) + yz
      vir(3,3) = vir(3,3) + zz         
  ENDDO
end subroutine intra_harm_harm

subroutine Morse_generic(dr,v,dv,Vo,sig,ro)
   IMPLICIT NONE
   REAL(dp), INTENT(in):: dr,Vo,sig,ro
   REAL(dp), INTENT(out) :: v,dv
   REAL(dp) :: expr
   expr=exp(-sig*(dr-ro))
   v=Vo*expr*(expr-2._dp)
   dv=-2._dp*Vo*sig*expr*(expr-1._dp)

end subroutine Morse_generic

subroutine Yukawa_generic(dr,v,dv,g,ro)
   IMPLICIT NONE
   REAL(dp), INTENT(in):: dr,g,ro
   REAL(dp), INTENT(out) :: v,dv

   v=g*exp(-dr/ro)/dr
   dv=-v*(1./dr + 1./ro)

end subroutine Yukawa_generic

subroutine Morse_oh(dr,v,dv)
   REAL(dp), INTENT(in):: dr
   REAL(dp), INTENT(out) :: v,dv
   REAL(dp) :: vy,dvy
   ! REAL(dp) :: Vo,sig,ro
   ! REAL(dp) :: expr

   call Morse_generic(dr,v,dv,vo_oh,sig_oh,ro_oh)

   call Yukawa_generic(dr,vy,dvy,g_oh,ry_oh)

   v=v+vy
   dv=dv+dvy
!    !Vo=0.35_dp/eV
!    Vo=0.25_dp/eV
!    !sig=6.13_dp*bohr
!    sig=9.78_dp*bohr
   !sig = 8.2656
!    ro=1.0_dp/bohr

!    expr=exp(-sig*(dr-ro))
!    v=Vo*expr*(expr-2._dp)
!   dv=-2._dp*Vo*sig*expr*(expr-1._dp)
 ! v=0
  !dv=0

end subroutine Morse_oh

subroutine Morse_oo(dr,v,dv)
   REAL(dp), INTENT(in):: dr
   REAL(dp), INTENT(out) :: v,dv
   REAL(dp) :: vy,dvy
   ! REAL(dp) :: g,Vo,sig,ro
   ! REAL(dp) :: expr

   call Morse_generic(dr,v,dv,vo_oo,sig_oo,ro_oo)

   call Yukawa_generic(dr,vy,dvy,g_oo,ry_oo)

   v=v+vy
   dv=dv+dvy

   ! Vo=0.15_dp/eV
   ! sig=4.0_dp*bohr
   ! !ro=2.6_dp/bohr
   ! ro=2.57_dp/bohr

   ! expr=exp(-sig*(dr-ro))
   ! !write(*,*) (dr-ro)*bohr
   ! v=Vo*expr*(expr-2._dp)
   ! dv=-2._dp*Vo*sig*expr*(expr-1._dp)

   ! g=8.5_dp/(ev*bohr)
   ! ro=5.0_dp/bohr

  ! v=v+g*exp(-dr/ro)/dr
  ! dv=dv-v*(1./dr + 1./ro)
 ! v=0
 !dv=0

end subroutine Morse_oo

subroutine Morse_cso(dr,v,dv)
   REAL(dp), INTENT(in):: dr
   REAL(dp), INTENT(out) :: v,dv
   REAL(dp) :: vy,dvy
   ! REAL(dp) :: Vo,sig,ro
   ! REAL(dp) :: expr

   call Morse_generic(dr,v,dv,vo_cso,sig_cso,ro_cso)

   call Yukawa_generic(dr,vy,dvy,g_oo,ry_oo)

   v=v+vy
   dv=dv+dvy


   ! Vo=0.1_dp/eV
   ! sig=1.0_dp*bohr
   ! ro=3.4604_dp/bohr

   ! expr=exp(-sig*(dr-ro))
   ! v=Vo*expr*(expr-2._dp)
   ! !write(*,*) (dr-ro)*bohr
   ! dv=-2._dp*Vo*sig*expr*(expr-1._dp)
!  v=0
 !  dv=0

end subroutine Morse_cso

subroutine Morse_cscs(dr,v,dv)
   REAL(dp), INTENT(in):: dr
   REAL(dp), INTENT(out) :: v,dv
   REAL(dp) :: vy,dvy
   ! REAL(dp) :: vo,sig,ro,dro
   ! REAL(dp) :: expr

   call Morse_generic(dr,v,dv,vo_cscs,sig_cscs,ro_cscs)
   call Yukawa_generic(dr,vy,dvy,g_cscs,ry_cscs)

   v=v+vy
   dv=dv+dvy
   ! Vo=1.0_dp/eV
   ! sig=10.0_dp*bohr
   ! ro=4.47_dp/bohr

   ! expr=exp(-sig*(dr-ro))
   ! v=Vo*expr*(expr-2._dp)
   ! !write(*,*) (dr-ro)*bohr
   ! dv=-2._dp*Vo*sig*expr*(expr-1._dp)
 !v=0
 !dv=0

end subroutine Morse_cscs

subroutine yukawa_csh(dr,v,dv)
   REAL(dp), INTENT(in):: dr
   REAL(dp), INTENT(out) :: v,dv
   ! REAL(dp) :: g,sig,ro,dro
   ! REAL(dp) :: expr

   call Yukawa_generic(dr,v,dv,g_csh,ry_csh)

   ! !g=8.5_dp/(ev*bohr)
   ! g=17.0_dp/(ev*bohr)
   ! ro=2.272_dp/bohr
   ! !ro=1.136_dp/bohr

   ! v=g*exp(-dr/ro)/dr
   ! dv=-v*(1./dr + 1./ro)
!  v=0
!  dv=0

end subroutine yukawa_csh

subroutine yukawa_hh(dr,v,dv)
   REAL(dp), INTENT(in):: dr
   REAL(dp), INTENT(out) :: v,dv
   ! REAL(dp) :: g,sig,ro,dro
   ! REAL(dp) :: expr

   call Yukawa_generic(dr,v,dv,g_hh,ry_hh)

   ! g=1.0_dp/(ev*bohr)
   ! ro=1.5_dp/bohr

   ! v=g*exp(-dr/ro)/dr
   ! dv=-v*(1./dr + 1./ro)
!  v=0
! dv=0

end subroutine yukawa_hh

!-------------------------------------------------------
subroutine build_Onblist(X)
   IMPLICIT NONE
   real(dp), intent(in)  :: X(:,:)
   integer :: iH,iO,i,j
   real(dp):: r2,r(3)
   

   O_n_nb(:)=0
   Onblist(:,:)=-1
   DO i=1,n_O
      iO=indices_O(i)
      DO j=1,n_H
         iH=indices_H(j)
         r2=SUM(r_pbc(X(iH,:)-X(iO,:))**2)
         !write(0,*) iO,iH,sqrt(r2)*bohr
        ! STOP
         if(r2<=rmax2_Onblist) then
            O_n_nb(i)=O_n_nb(i)+1
            !if(O_n_nb(i)>2) write(0,*) "Warning: 3 neighbours for O at index "//int_to_str(iO)
            if(O_n_nb(i)>OMAXNB) THEN
               WRITE(0,*) "Error: too many neighbours for O at index "//int_to_str(iO)     
               STOP "Execution stopped"
            endif
            Onblist(O_n_nb(i),i)=iH
         endif
      ENDDO
   ENDDO

end subroutine build_onblist

function r_pbc(r)
   REAL(dp), INTENT(IN), DIMENSION(3) :: r
   REAL(dp), DIMENSION(3) :: r_pbc

   r_pbc=matmul(simulation_box%hcell_inv,r)
   r_pbc=r_pbc-nint(r_pbc)
   r_pbc=matmul(simulation_box%hcell,r_pbc)

end function r_pbc

end module ff_cso2h3
