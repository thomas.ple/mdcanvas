module potential_dispatcher
	USE kinds
	USE nested_dictionaries
	USE string_operations
	USE model_potentials
	USE qtip4pf_potential
	USE h2o_pat
	USE ff_co2
	USE ff_cso2h3
	USE file_handler, only: output
	USE diatomic
	USE potential
	USE atomic_units
  IMPLICIT NONE

	CHARACTER(:), allocatable, SAVE :: pot_name
	INTEGER, SAVE :: n_threads

	PRIVATE
	PUBLIC :: initialize_potential, finalize_potential
	
contains

	subroutine initialize_potential(param_library,nat,ndim)
		IMPLICIT NONE
		TYPE(DICT_STRUCT), INTENT(IN) :: param_library
		INTEGER, intent(in) :: nat,ndim
		INTEGER :: i
		CHARACTER(:), allocatable :: xyz_file
		LOGICAL :: file_exists

		
		pot_name = param_library%get("POTENTIAL/pot_name")
		!! @input_file POTENTIAL/pot_name (potentials)
		pot_name=to_upper_case(trim(pot_name))

    hessian_available=.FALSE.
		
    SELECT CASE(pot_name)
		CASE("QTIP4PF")
			call initialize_simulation_box(param_library,ndim)
			write_pot_components =param_library%get("POTENTIAL/write_pot_components",default=.FALSE.) 
			if(write_pot_components) then
				n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)
				write_stride=param_library%get("POTENTIAL/write_stride",default=1) 
				allocate(write_now(n_threads) &
								,unit_pot(n_threads) &
								,write_count(n_threads) &
								,vlj_mean(n_threads) &
								,vewald_mean(n_threads) &
								,vstretch_mean(n_threads) &
								,vbend_mean(n_threads) &
				)
				vlj_mean=0; vewald_mean=0; vstretch_mean=0; vbend_mean=0
				write_count=0
				write_now=.FALSE.
				DO i=1,n_threads
					open(newunit=unit_pot(i),file=output%working_directory//"/pot_components_"//int_to_str(i)//".out")
					if(nat>3) then					
						write(unit_pot(i),*) "#V_lj   V_coulomb   V_stretching    V_bending"
					else
						write(unit_pot(i),*) "#V_stretching    V_bending"
					endif
				ENDDO
			endif
			get_pot_info => get_qtip4pf_pot_info
			Pot => Pot_qtip4pf
			dPot => dPot_qtip4pf
			compute_hessian => hessian_qtip4pf
      hessian_available=.FALSE.
		CASE("H2O_PAT")
			!call initialize_simulation_box(param_library,ndim)			
			get_pot_info => get_h2opat_pot_info
			Pot => Pot_h2opat
			dPot => dPot_h2opat
			compute_hessian => hessian_h2opat
      hessian_available=.TRUE.
			call initialize_h2opat()
		CASE("FFCO2")
			call initialize_simulation_box(param_library,ndim)
			get_pot_info => get_ffco2_pot_info
			Pot => Pot_ffco2
			dPot => dPot_ffco2
			compute_hessian => hessian_ffco2
      hessian_available=.FALSE.
		CASE("CSO2H3")
			call initialize_simulation_box(param_library,ndim)
			get_pot_info => get_cso2h3_pot_info
			Pot => Pot_cso2h3
			dPot => dPot_cso2h3
			compute_hessian => hessian_cso2h3
			call initialize_cso2h3(param_library)
      hessian_available=.FALSE.
		CASE("DIATOMIC")
			get_pot_info => get_diatomic_pot_info
			Pot => Pot_diatomic
			dPot => dPot_diatomic
			compute_hessian => hessian_diatomic
			call initialize_diatomic(param_library)
      hessian_available=.FALSE.
		CASE DEFAULT
			call initialize_model_potentials(param_library,nat,ndim,pot_name)
		END SELECT

	end subroutine initialize_potential


	subroutine finalize_potential()
		IMPLICIT NONE
		INTEGER :: i

		SELECT CASE(pot_name)
		CASE("QTIP4PF")
			if(write_pot_components) then
				DO i=1,n_threads
			 		close(unit_pot(i))
				ENDDO
			endif
		END SELECT

	end subroutine finalize_potential

	subroutine initialize_simulation_box(param_library,ndim)
	!! INITIALIZE PERIODIC BOUNDARIES AND SIMULATION BOX
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		INTEGER :: ndim, i,j,INFO
		REAL(dp), allocatable :: box_abc(:),hcell(:),hcellin(:,:)
		INTEGER, allocatable :: ipiv(:)

		if(ndim/=3) then
			write(*,*) "Warning: ndim must be 3 for PBC! Ignoring box input."
			return
		endif

		!if(.NOT. simulation_box%use_pbc) return
		
		allocate(simulation_box%hcell(ndim,ndim))
		allocate(simulation_box%hcell_inv(ndim,ndim))
		simulation_box%ndim=ndim
		
		if(param_library%has_key("BOUNDARIES/box_abc")) then
			box_abc=param_library%get("BOUNDARIES/box_abc")
			simulation_box%a=box_abc(1)
			if(ndim>1) simulation_box%b=box_abc(2)
			if(ndim>2) simulation_box%c=box_abc(3)
		else
			simulation_box%a=param_library%get("BOUNDARIES/box_a",default=1.)
			simulation_box%b=param_library%get("BOUNDARIES/box_b",default=simulation_box%a)
			simulation_box%c=param_library%get("BOUNDARIES/box_c",default=simulation_box%a)
		endif
		
		if(param_library%has_key("BOUNDARIES/hcell")) then
			hcell=param_library%get("BOUNDARIES/hcell")
			if(size(hcell)/=ndim*ndim) STOP "Error: BOUNDARIES/hcell does not match ndim!"
			simulation_box%hcell=RESHAPE(hcell,(/ndim,ndim/))
			!DO i=1,ndim ; DO j=i+1,ndim
			!	if(simulation_box%hcell(j,i)/=0) STOP "Error: BOUNDARIES/hcell must be triangular superior!"
			!ENDDO; ENDDO
			simulation_box%a=SQRT(SUM(simulation_box%hcell(:,1)**2))
			if(ndim>1) simulation_box%b=SQRT(SUM(simulation_box%hcell(:,2)**2))
			if(ndim>2) simulation_box%c=SQRT(SUM(simulation_box%hcell(:,3)**2))
		else
			simulation_box%hcell=0
			simulation_box%hcell(1,1)=simulation_box%a
			simulation_box%hcell(2,2)=simulation_box%b
			simulation_box%hcell(3,3)=simulation_box%c
		endif

		simulation_box%box_lengths(1) = simulation_box%a
		simulation_box%box_lengths(2) = simulation_box%b
		simulation_box%box_lengths(3) = simulation_box%c

		!write(*,*) simulation_box%a*bohr, simulation_box%b*bohr, simulation_box%c*bohr
		write(*,*) "CELL MATRIX   | CELL MATRIX INVERSE"
		!simulation_box%hcell_inv=ut3x3_mat_inverse(simulation_box%hcell)
	  allocate(hcellin(ndim,ndim),ipiv(ndim)) ; hcellin=simulation_box%hcell
    simulation_box%hcell_inv=0._dp
    DO i=1,ndim
      simulation_box%hcell_inv(i,i)=1._dp
    ENDDO
    CALL DGESV(ndim,ndim,hcellin,ndim,ipiv,simulation_box%hcell_inv,ndim,INFO)
    do i=1,ndim
			!simulation_box%hcell_inv(i,i)=1._dp/simulation_box%hcell(i,i)
			write(*,*) simulation_box%hcell(i,:)*au%bohr, "  |  ", simulation_box%hcell_inv(i,:)/au%bohr
		enddo
	
	end subroutine initialize_simulation_box
    
end module potential_dispatcher
