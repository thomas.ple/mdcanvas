PROGRAM MDcanvas
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
  use potential_dispatcher
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  IMPLICIT NONE
	TYPE(DICT_STRUCT) :: param_library	

  call initialize_framework(param_library)
	
  !----------------------------------------
  ! SIMULATION INITIALIZATION GOES HERE


  call open_output_files(param_library)

  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE
  write(*,*) "Hello World!" 

END PROGRAM MDcanvas
